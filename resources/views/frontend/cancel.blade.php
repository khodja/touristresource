@extends('layouts.frontend')

@section('content')
@include('front_partials.nav-light')
@include('front_partials.nav-primary')
<div class="change">
    <div class="container">
        <div class="row">
            <form action="{{ action('PageController@setCancel') }}" method="POST" class="dates__item">
                @csrf
                <div class="confirm">
                    <div class="top">Cancel reservation</div>
                    <div class="bottom">After canceling a reservation, it cannot be changed or restored.</div>
                </div>
                @foreach($bookings as $booking)
                <div class="change__top no-shadow d-flex flex-wrap flex-lg-row flex-column align-items-center">
                    <div class="col-lg-5 py-0 left d-flex align-items-center">
                        <img src="{{$booking->room->image[0]}}" alt="photo" />
                        <div class="titles">
                            <div class="name">{{$booking->room->room_type->name_ru}} {{$booking->room->room_name->name_ru}}</div>
                            <div class="hotel">{{$booking->name}}</div>
                        </div>
                    </div>
                    <div class="col-lg-5 py-0 center">
                        <div class="date">{{\Carbon\Carbon::parse($booking->from)->isoFormat('MMMM D,YYYY') .' — '.\Carbon\Carbon::parse($booking->to)->isoFormat('MMMM D,YYYY')}}</div>
                        <div class="days">{{ $booking->getNights() }} days</div>
                    </div>
                    <div class="col-lg-2 py-0 right">
                        <div class="price">USD {{number_format((float)$booking->price, 2, '.', '')}}</div>
                        <div class="persons">{{$booking->adult}} persons</div>
                    </div>
                    <input type="hidden" name="booking_id[]" value="{{$booking->id}}">
                </div>
                @endforeach
                <div class="buttons d-flex flex-lg-wrap flex-column align-items-center justify-content-center">
                    <button class="dates__confirm">Confirm</button>
                    <a href="{{ URL::previous() }}" class="dates__cancel">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection
