@extends('layouts.frontend')

@section('content')
@include('front_partials.nav-light')
<div class="nav-light">
    <div class="container">
        <div class="row d-flex">
            <div class="col-4">
                <a href="{{action('PageController@show', json_decode($parsed_rooms,true)[0][0])}}" class="nav-light__links d-flex align-items-center justify-content-lg-start justify-content-center">
                    <i data-feather="home"></i>
                    <span class="md-hide">Object choice</span>
                </a>
            </div>
            <div class="col-4">
                <div class="nav-light__links link-active d-flex align-items-center justify-content-center">
                    <i data-feather="lock"></i>
                    <span class="md-hide">Reservation</span>
                </div>
            </div>
            <div class="col-4">
                <div class="nav-light__links d-flex align-items-center justify-content-lg-end justify-content-center">
                    <i data-feather="check-circle"></i>
                    <span class="md-hide">Confirmation</span>
                </div>
            </div>
        </div>
    </div>
</div>
<reservation></reservation>
@endsection

@section('script')
<script src="{{asset('js/countrySelect.min.js')}}"></script>
<script>
    Vue.component('reservation', {
        data() {
            return {
                rooms: {!! $booking_rooms !!},
                checkedRooms: {!! $parsed_rooms !!},
                nights: {{ $nights }},
                mergedBookings: []
            }
        },
        mounted() {
            this.mergeBookings();
        },
        methods:{
            getRoom: function(roomId){
                return this.rooms.find(e => parseInt(e.id) === parseInt(roomId));
            },
            removeRoom: function(roomArray){
                this.checkedRooms = this.checkedRooms.filter(e => JSON.stringify(e) !== JSON.stringify(roomArray));
                this.mergeBookings();
                return 1;
            },
            precisionRound: function (number) {
                let factor = Math.pow(10, 2);
                return (Math.round(number * factor) / factor).toFixed(2);
            },
            mergeBookings: function(){
                let finalItems = [];
                this.rooms.forEach((room) => {
                    let resulting = this.getRoomFiltered(room.id);
                    if(resulting) finalItems.push(resulting);
                });
                this.mergedBookings = finalItems;
                 return 1;
            },
            getRoomFiltered: function(roomId){
                let filtered = this.checkedRooms.filter(e => parseInt(e[1]) === parseInt(roomId));
                if(filtered.length > 0){
                    let filteredArray = [filtered[0][0],filtered[0][1],0,0,0];
                    filtered.map(room => {
                        filteredArray[2]+=parseInt(room[2]);
                        filteredArray[3]+=parseInt(room[3]);
                        filteredArray[4]+=parseInt(room[4]);
                    });
                    return filteredArray;
                }
                return null;
            }
        },
        computed: {
            getPrice: function () {
                let price = 0; // [hotelId , roomId, roomCount, personCount, price]
                this.checkedRooms.map(e => {
                    price = e[4] ;
                });
                return this.precisionRound(price);
            },
            getPay: function () {
                let price = (this.getPrice/100)*2.5;
                return this.precisionRound(price);
            }
        },
        template: `
<div class="reservation">
    <div class="container">
        <form method="POST" action="{{action('PageController@book')}}" class="row d-flex">
            @csrf
            <div class="col-xl-8 col-lg-7 col-md-12">
                <div class="reservation__info d-flex flex-wrap white-box">
                    <div class="left">
                        <img src="{{$hotel->image[0]}}" alt="{{$hotel->name}}" title="{{$hotel->name}}" />
                    </div>
                    <div class="right d-flex flex-column justify-content-between">
                        <div class="top title">{{$hotel->name}}</div>
                        <div class="bottom d-flex align-items-end justify-content-between">
                            <div class="left">
                                <div class="d-flex align-items-end">
                                    <img src="{{asset('images/location.svg')}}" alt="" width="50">
                                    <div class="address">{{ $hotel->address }}</div>
                                </div>
                            </div>
                            <div class="right d-flex">
                                <div class="star d-flex align-items-center">
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="rating">8/10</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="reservation__box white-box">
                    <div class="title">Personal information</div>
                    <div class="input">
                        <input type="hidden" name="period" value="{{$from .' , '. $to}}"  />
                        <label for="name">First and last name</label>
                        <input type="text" name="name" id="name" value="{{Auth::user()->name ?? ''}}" @if(Auth::check()) disabled @endif required="required" />
                        <div class="addition">use latin alphabet</div>
                    </div>
                    <div class="input">
                        <label for="country">Country</label>
                        <input type="text" name="country" id="country" value="{{Auth::user()->country ?? ''}}" @if(Auth::check()) disabled @endif required="required" />
                    </div>
                    <div class="input">
                        <label for="email">E-mail</label>
                        <input type="text" name="email" id="email" value="{{Auth::user()->email ?? ''}}" @if(Auth::check()) disabled @endif required="required" />
                        <div class="addition">We will send a booking confirmation by email</div>
                    </div>
                    <div class="input">
                        <label for="phone">Phone number</label>
                        <div class="input-tel">
                            <i data-feather="phone"></i>
                            <input type="tel" name="phone" id="phone" value="{{Auth::user()->phone ?? ''}}" @if(Auth::check()) disabled @endif required="required" />
                        </div>
                        <div class="addition">The property may require your phone number to make sure the reservation is valid.</div>
                    </div>
                </div>
                <div class="reservation__box reservation__item white-box" v-for="room in checkedRooms">
                    <div class="title">@{{ getRoom(room[1]).room_type.name_en }} @{{ getRoom(room[1]).room_name.name_en }}</div>
                    <div class="remove" v-on:click="removeRoom(room)">
                        <i data-feather="x"></i>
                    </div>
                    <div class="input">
                        <label for="guest_name[]">Guest Name</label>
                        <input type="text" name="guest_name[]" id="guest_name[]" required="required" />
                        <input type="hidden" name="room_info[]" :value="room" />
                        <div class="addition">use latin alphabet</div>
                    </div>
                </div>
                <div class="reservation__complete md-hide">
                    Completing this booking you agree to the booking conditions, general terms, and privacy policy.
                </div>
                <button class="reservation__button md-hide">Pay and complete booking</button>
            </div>
            <div class="col-xl-4 col-lg-5 col-md-12">
                <div class="reservation__checkout white-box">
                    <div class="top d-flex align-items-center">
                        <div class="icon d-flex align-items-center"><svg width="20" height="20" viewBox="0 0 20 20"
                             fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11.4659 6.67711C11.3933 6.61901 11.3014 6.59038 11.2087 6.59686H9.79569L11.1152 5.12225L11.2388 4.98515L11.3189 4.88818C11.3409 4.85717 11.3589 4.82344 11.3724 4.78787C11.3875 4.74954 11.3954 4.7087 11.3958 4.66749C11.4051 4.56998 11.3568 4.47593 11.2722 4.42674C11.1623 4.37646 11.042 4.35351 10.9214 4.35986H9.26456C9.17704 4.35388 9.09035 4.38005 9.0207 4.43342C8.96317 4.48245 8.93114 4.5551 8.93385 4.63071C8.93385 4.7444 8.9706 4.81127 9.04743 4.83802C9.15599 4.86879 9.26873 4.88233 9.38147 4.87815H10.4504C10.4059 4.94055 10.3469 5.01412 10.2734 5.09884L9.99611 5.41316L9.63535 5.81441L9.20443 6.29258C9.05411 6.45976 8.95724 6.57345 8.91047 6.62695C8.80441 6.7559 8.81761 6.94537 8.94053 7.0583C9.02897 7.1299 9.141 7.16572 9.25454 7.15862H11.2054C11.2987 7.16593 11.3915 7.13856 11.4659 7.08171C11.5227 7.0321 11.5545 6.95983 11.5528 6.88443C11.5556 6.80589 11.5239 6.73007 11.4659 6.67711Z" fill="#00236D"/>
                        <path d="M14.4118 8.35912C14.3533 8.31231 14.2793 8.28961 14.2047 8.29559H13.0823L14.1345 7.11857L14.2347 7.00822L14.2982 6.93131C14.316 6.90653 14.3307 6.87957 14.3416 6.85106C14.3541 6.82022 14.3609 6.78736 14.3617 6.75409C14.3701 6.67606 14.3322 6.60028 14.2648 6.56015C14.177 6.51977 14.0807 6.50138 13.9842 6.50665H12.6748C12.6051 6.50276 12.5363 6.52412 12.481 6.56684C12.4348 6.60571 12.409 6.66364 12.4109 6.724C12.4109 6.81428 12.4409 6.86778 12.5011 6.89119C12.5878 6.9166 12.678 6.92793 12.7683 6.92463H13.6101C13.5733 6.97478 13.5266 7.03163 13.4698 7.10185L13.2493 7.35263L12.962 7.68702L12.628 8.06821C12.5077 8.20196 12.4287 8.29337 12.3908 8.3424C12.3121 8.44347 12.3222 8.58767 12.4142 8.67678C12.4834 8.72506 12.5673 8.74755 12.6514 8.74032H14.208C14.2821 8.74625 14.3558 8.72485 14.4151 8.68013C14.4593 8.64004 14.4837 8.58261 14.4819 8.52297C14.4835 8.46077 14.4579 8.40096 14.4118 8.35912Z" fill="#00236D"/>
                        <path d="M17.9132 6.48991C17.8406 6.43181 17.7487 6.40318 17.656 6.40965H16.2463L17.5658 4.93504L17.6861 4.78457L17.7662 4.6876C17.7882 4.6565 17.8061 4.62281 17.8197 4.58728C17.8348 4.54895 17.8427 4.50812 17.8431 4.46691C17.8524 4.36939 17.8041 4.27535 17.7195 4.22615C17.6055 4.16981 17.479 4.14335 17.352 4.14924H15.7152C15.6277 4.14327 15.541 4.16943 15.4714 4.22281C15.4138 4.27184 15.3818 4.34448 15.3845 4.42009C15.3845 4.53378 15.4212 4.60066 15.4981 4.62741C15.6066 4.65813 15.7194 4.67167 15.8321 4.66753H16.9011C16.8565 4.72994 16.7986 4.8035 16.7274 4.88822L16.4501 5.20254L16.0893 5.6038L15.6584 6.08196C15.5081 6.24915 15.4112 6.36284 15.3645 6.41634C15.3209 6.46922 15.2962 6.53509 15.2943 6.60359C15.2898 6.6958 15.3266 6.78525 15.3945 6.84769C15.4826 6.91415 15.5919 6.94621 15.7018 6.93797H17.6527C17.746 6.94529 17.8388 6.91791 17.9132 6.86107C17.97 6.81145 18.0018 6.73919 18.0001 6.66378C17.9939 6.59695 17.9629 6.53488 17.9132 6.48991Z" fill="#00236D"/>
                        <path d="M16.6617 12.0468C16.5761 12.0062 16.4773 12.004 16.3899 12.0406H16.3933C12.7723 13.5398 8.5 13.8403 6.5 9C5.74034 7.16154 6.41087 4.2806 7.25049 2.47732C7.32937 2.31039 7.25809 2.11105 7.09132 2.0321C7.00572 1.99155 6.90693 1.98934 6.81957 2.02591C2.85027 3.6581 0.954278 8.20225 2.58478 12.1756C4.21529 16.1489 8.75485 18.0468 12.7242 16.4146C14.5346 15.6702 15.9976 14.2694 16.8209 12.492C16.8997 12.3251 16.8284 12.1257 16.6617 12.0468Z" fill="#00236D"/>
                        </svg></div>
                        <div class="head-title">@{{ this.nights }} nights</div>
                    </div>
                    <div class="item d-flex justify-content-between" v-for="room in mergedBookings">
                        <div class="left d-flex flex-column justify-content-between">
                            <div class="name">@{{ getRoom(room[1]).room_type.name_en }} @{{ getRoom(room[1]).room_name.name_en }}</div>
                            <div class="persons">max. @{{ getRoom(room[1]).capacity }} persons</div>
                        </div>
                        <div class="right d-flex flex-column align-items-end justify-content-between">
                            <div class="count">x@{{ room[2] }}</div>
                            <div class="price">USD @{{ room[4] }}</div>
                        </div>
                    </div>
                    <div class="check d-flex justify-content-between align-items-center">
                        <div class="left">Check-in</div>
                        <div class="right">{{\Carbon\Carbon::parse($from)->isoFormat('MMMM D, YYYY')}}</div>
                    </div>
                    <div class="check d-flex justify-content-between align-items-center">
                        <div class="left">Check-out</div>
                        <div class="right">{{\Carbon\Carbon::parse($to)->isoFormat('MMMM D, YYYY')}}</div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <a href="{{ URL::previous() }}" class="btn text-center">Change</a>
                    </div>
                    <div class="price-info">
                        <div class="box d-flex align-items-center">
                            <div class="icon d-flex align-items-center"><svg width="20" height="20" viewBox="0 0 20 20"
         fill="none" xmlns="http://www.w3.org/2000/svg">
    <g clip-path="url(#clip0)">
    <path d="M10 0.833313V19.1666" stroke="#00236D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M14.1667 4.16669H7.91667C7.14312 4.16669 6.40125 4.47398 5.85427 5.02096C5.30729 5.56794 5 6.30981 5 7.08335C5 7.8569 5.30729 8.59877 5.85427 9.14575C6.40125 9.69273 7.14312 10 7.91667 10H12.0833C12.8569 10 13.5987 10.3073 14.1457 10.8543C14.6927 11.4013 15 12.1431 15 12.9167C15 13.6902 14.6927 14.4321 14.1457 14.9791C13.5987 15.5261 12.8569 15.8334 12.0833 15.8334H5" stroke="#00236D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
    </g>
    <defs>
    <clipPath id="clip0">
    <rect width="20" height="20" fill="white"/>
    </clipPath>
    </defs>
    </svg></div>
                            <div class="head-title">Pay in object</div>
                        </div>
                        <div class="price">USD @{{getPrice}}</div>
                        <div class="description">The total amount that you pay when placing in the object. All taxes are included.</div>
                    </div>
                    <div class="price-info">
                        <div class="box d-flex align-items-center">
                            <div class="icon d-flex align-items-center"><svg width="20" height="20" viewBox="0 0 20 20"
         fill="none" xmlns="http://www.w3.org/2000/svg">
    <g clip-path="url(#clip0)">
    <path d="M10 0.833313V19.1666" stroke="#00236D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M14.1667 4.16669H7.91667C7.14312 4.16669 6.40125 4.47398 5.85427 5.02096C5.30729 5.56794 5 6.30981 5 7.08335C5 7.8569 5.30729 8.59877 5.85427 9.14575C6.40125 9.69273 7.14312 10 7.91667 10H12.0833C12.8569 10 13.5987 10.3073 14.1457 10.8543C14.6927 11.4013 15 12.1431 15 12.9167C15 13.6902 14.6927 14.4321 14.1457 14.9791C13.5987 15.5261 12.8569 15.8334 12.0833 15.8334H5" stroke="#00236D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
    </g>
    <defs>
    <clipPath id="clip0">
    <rect width="20" height="20" fill="white"/>
    </clipPath>
    </defs>
    </svg></div>
                            <div class="head-title">Pay now</div>
                        </div>
                        <div class="price">USD @{{ getPay }}</div>
                        <div class="description">At the moment, you will need to pay only 2.5% of the total reservation cost, the rest — when placing in the object.</div>
                    </div>
                    <div class="payment d-flex align-items-center">
                        <div class="payment__item"><svg width="44" height="44"
     viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<rect x="2" y="15" width="40" height="13" fill="url(#pattern0)"/>
<defs>
<pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
<use xlink:href="#image0" transform="translate(-0.002125) scale(0.00325 0.01)"/>
</pattern>
<image id="image0" width="309" height="100" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATUAAABkCAMAAADQZFfQAAADAFBMVEUAAADe3On6+vzOzdzLy9v09PjIyNfs6/GQjrfw8PXX1uK7us/CwdPPz93u7vN1c57Z2OWhobrS0t/29vnJydn5+ftnZZSurcTb2+ff3un4+PuDgaeSkbGcnLehoLy/vdTo6PCko72lpbqpp8i9vc7m5u5RToSNirKko73i4us3MYBPSpG1s863t8s9O3SpqMGkpLivr8U4MYE3MX42MnY7OmtGRHpSTJBVVYB7eps+O3pRTI9MSIR9eqeTkLumo8a7udPk5O0wLmwyMmE2NG9JSnRQT35bWYpZWYBgXpFhYYdycZaDf62ZmLWlo8Kuq8otLGMzL3Q9N4A6Nnc4OGVBPII+Pm1FP4dCPn9BPndGQoBERHBYU5RdWZRjXppmZopsaZx+fqCGgrKEg6OJhq6QjLqLi6i5uM8vL2A1NWhJQ4tVUopYVI1oY59vbJ9ubpN/e66SjryPjquUk7Cbmb2em8C3tc61tMczL3U2MXkyMWlIR3hdW4tdXYRtaaNpaYxybaZ1dZaYlb87NIU3Nm8/QGtKRYlNTXZVT5RPUHVeWpNlYpZkYZGGhqE2MH8xLm8+N4ZIRIFfWZx1cKd6dqk2LoIzMHI1NmJDPIo1L345MYU0Lno1MXQxMWU3MX0zMWs4MYE0MXEzMWw4MYMwMV80MXIxMWIxMWE1MXc1MXY3MXw3MYAyMWYzMW0wMVw2MXs3MX8yMWcwMV42MXg5MYUpJHMsJH0sKmknJWI1L38sK2IkJFkqJm0nJGYsJ3ApJGwuL1wpJHApJ2YxL2krJHcwLXAzK34wKnwwKnksJnQpKF8lJF0qK1clJlQzK4EvJ38sJHotKW4vLG0tK2YmJGQmJGEmJ1kkJVcuJ3wrJHUzL3QqJ2ovLWg2L4MzLngvKnQyL20sKWspJ2MnJl8kJFsyLnIoJGkrKmQxLXUwLmwnKFY0LXsuKnEtLGAuLl4nJlwuJ3kiI1UjJFIwL2UqKlwsLVouKHUuLWQpKlkkJVMsLV0aGVQhHGsgGHQeGmAaGFtaaI+RAAAAnHRSTlMAGwIwNAc4EHULJ0Y+LQ2dJGUrBToDr1YhHgSMemtoRBNwX11CFc5/bBn02E9L6ltaU/X08uvez8eW59TUlHlkShf79fTVzsHBt7ShjnBgV/r17u7t5+bj4uLd3cjBuq+qkIqJhYSAUPry4MnCs6amlX53cnBsVE/69/fZvbqtqqebdPXy4dvQzsm6tLKF+ffw2sKhm/z68er8+PnVzCy9AAAUaUlEQVR42tSbaVSUVRjHH2CAAwzDGrgRHAE7lFictJRE0bK0RE1wgVBTK1TCTA3LJa00w7JjZpnlIARqYmbpIR1lb1hkUcBkc2FHZVEQEfD0oYuD3Zl5n/e+743Th34f/OAZzxl/vM/z5/7nDvy/Mbf3DQr4IvIr/9Gvvjp6tH/YiB12jz2p8Ib/GiczZy8b34ho16f6iY6OWOfra+PlpXR2drbow7kPha29txNw4WiheIDZQ2z7cCRY6b1I0Y+ZHn0vGwISeLsPcxkfnF1Skp3deHB/H1VVjdmtra3vz387wNPRCfjwsFY4Oyu9bAIjoqOf9OxjZnSEu/MgwNjZW9hcROi900/RPxQa0LUs5LVNO73kvxmf5flnBFzvY5Y9fa9b6y4+5JQeWaHmwMI6aFrw5fSS7Mb9RhwhVLWmZFaNt/O0B7koloTP3pdH0LYTtDqK29srip81BYTCroxfRMgwpqunt3mKA8hkTYO6j591/KbjD0LdWPpOlHXkLw7o+PEBNQ/IcgFxTNxe+rapNlv3gCHWCD8duZGW+UHkdF8fU5DEZGVeZZ4mVqPRxP6q46iOuDjNJz6AEBPSfKeZmJPkhI6ejpctQQ6PbD9jYI1SoCc+6JLOGLV27AF/DhN/LuyCm0riDxJY1vo4Up2ZNtodpDCbVxnbj8BaXAX+772VaxZtK7zTnCHPWmJiR4wVyECZr8atdY7xoK96p45K07eW5S7mzKW0KZsYk2GNcPKnG44gwaA5ebFG1o5Sa8UTQAwri9VTn+8t6pK2lkjo+Ahk8MptfWu/UerH0RcN2XARtXZzqw/+P7QrvRwfT6UJ0ZdGrN0IM5eaiQX39KQJrFV8yB7upzaHMMSdoNZOlFmAJJOWd4oM6CxbPQvr/0CtnRqBriO34KZ4Ik3yUaPWWl1AgkeTY1nWcp5wkgpfv80hZFTZ1gjli0CSoAaRAc1/GSjrTh9ArWXZYQ/atNTseE5rmY8BG8s5LUxrR7+UkcTewzf9cqdLwtqt5weDBKYkC3Br9TZAeUMsDB4FATajyHDyWksLBDZraRIgYUAoHglycNy57U4hwxqhw0EyC+rVuLXrL5oCZRoeBjWnFMLprC2N57V28toHg4GJ+actQmtHDay9CfJQDY8pKsTDQEf5OzKyAA+DguFAsQq9gofBKBUYMSy1Jp7fWvVzjwCTkZWxEtZyF4JcTP1i6JwKrd2dIvFmvEO6cWvdoVZAMbsoEgYbhdKO9TvjitC0lcBmYbuBNMSaZp4VyGaI295mdEB1i80EmLidFRnQgrdAjxmX8DBIHQqGOFBpfGGwFpjYXotlWyPkmAEH1tubcWuEMiX7UX0xH7fWud5gz4w7jYdBqisYoJysG09ua61KYLKqUtpasSfw4LOtUMxahxuwsLitxq3VGy7EsddRaz9Otjac91BdEHBbuzZ7EnuRzG5hWIvrt7YKuPBqzsDWGqGcfTz4SMzaaSXoMWkrvtauPONkGC2p1BlfGEQCk/eSYyXCgJATDny83Cxi7e5YYOBBsgANgzO6Xzto4YGvNaPCw6aJKuMMgz3s3f1cntSAEq7u8uB82IqwASXcH6MCcRzO4o8abTvwwoOAFB5DRtQi0mRZy5wILGzOJUlaI1xw5ix6X+vCrBFu2TKyYGw+bq1zDC2ZaOEhDIMsG9DDLzWeaa2xJCU9PYVA/kzLrtK3Vq0AFlH3ZFkrfg/4GFckYq3clVGMEmmotfrFoIMWHpi1my+YAMU08hTDWnZ6if+CPcMmukZEzJw4IeDz8cGt6Wk3HobBHBUwsP/heKxkGBAqvgY+1olZ63iLobpBxNosR8N3LavwUF6OR63pnIUNVVoaPOc+yrWvz0/TiateACyWVCZJhwEh5zlT4MLk+QwkDAhli8SzYEy3Gg2D/E1A4C087LJErV0Om2EFCCr3gLC0tKqTmdOBgWpOixxrhE8GAx8xhbi1++JnKocGNW6t3g8MWIpYExQeKv+b8SIRmm5nKd6zjlwZnPnXTGAwIzmJvdboiAbyLrZeZEAJt/b6gAhjRdZa9zNGterG67g1g8LDV/RRSx8q0YvOcLFmnV/C8+RaK34X+HAVsXZY9Exllt+JWytYDRRaeAjDYJQlUIamilgrmQYDwvleEtNaHKUiCvgYtCwDt1b+KCMLMGud62kysguPl0CPyFOYNUKpBQyILeeS2GuNcn6FOXBhur0HW2vE2jhAsRzTbWwNO4ISHOQUHpajanBr2WFOMBBMnm7BrGkwa1e/seZebLi1nqmAMvysGreWT0caLzwIwsLD9uYx3FppJAyICZXYgO57XGiNUDwT+PDrRQf08K25eFk3NR8bUHoEpYy9KLAmLDzcxX5bG+CzZr7iGmItL2pPO2ptCXD/xoZaS0x0RLOgrRO3Ro+gtPBAw+CKQdS6ikXowZIB7bWZ55IQa9pATy1m7cJu4CSmB7V2uGwdICxuUCPW6BGU4lV3gCJaeDwpFqH7S1xgAOygCUqt5cxzssil0iia71TAx9Je3Fo5dqZSzW3DrRUsBiPWMAsPaWv7UxzgX2N2LQmxpp0OHrs01BrlvAL48C3CwoA8a5tByFNn1WgY/DxLMM8uwnINueERKH4KbZzsB/+WgEpkQDW5xE14LmateCLw4b03A7V2d4oplgWYNfqBO8VpAxIGgsIDFKXHRCvJxtIg4IQ234i13HBTgFXtiLWEij3AydRm4YASEvcKO3nbtkP4gNbTJUgLD6E1WnjQzKgRr4ka0183AR5o852EWNP29WgTtZi1858BJzt7EWuEu0rhDmxQo9bObHACI/xOH6CI3/AwHVHKqiRT/GcAB7T5RqxpHjcBAEUuZu3qrknA+5kLbq3cQVA0zG3DrRWsFgpGrGE3PAKaWNZIw7bRAnixuZeEhEH7h0Cw/E4jXGsJCRfcgQ/LkAzhWiOUCc5Urg1q1FrnclqQSBYeRhICcWuUkmw7e+AjCrWmHQl97M7FrP3+MXCyqRm1dneq4IX5ajQM6l8BY1Sjrgit0cKDYjVf6rPQgymjhyLrjdl8IwOa038zYXqxcEATEiq2ACerO7ABJWcqFRjgOOsQbi1fOESKiyJh8BIY8UYqWxqhKiX4MQ/AwZtvxJp2FTzAU4tZO//EEN4mqgezRrQ5IlmADOgZeucbKTwISOFBGfxCDduaLk3nu6FtDt58Y9by+n+0tjmYtavfDwI+VHO7UGvlfoaztK0Nt0YvX1HsxMKAFh60l0SsId7Gu4IsPJOpNEruwy8WWM3LEaw1QsVI4P4IHrEmOFP5nVWj1rrnqpDrM1KFB8XDv1TWZYXWlB0KkEF4HmZNOwH6WZiLWSOX/zgJ6hCGAcHoc6pNt3FrBUtBgI9x4UGgNzyMcE2VecUjrXG6pZzmG7GmoddvlxRj1i5EASeKHtTa/SlDgGK97BBmjRxBkSLUve4Aag3/SotdqswrHlXpYZKD9PU5zFreQloiGVtL6OP8CiveCm9bl2BACYkGl5p34gOKHEEJwy7h1vCvtFiNuHxQ5m2i6rQAc6nmG7Ompcdzx32YtasaW+Bkc7PQGqHMS99sG26t3heEuIiVazaAMTi0VvYdrPRIe2bznXwckdYy21uv5s0RWCP87gmcuHWg1sqDxLKAWhMeQQlOz1wRWKOFB4Ktf61cayfT/JWsO995mLX2LUCJyo3DrK0CTmy78GftFb2cvY1bK1gDQqwnMwoPFLPQWnnWCNWvii+3kcnH0QENBMqbWsxazm4A7htZf1d3nqFNBmEcP1fUxD3jqts4Ke5V996Ie6+6Z917gop7I4KIqMGBBNQEtTUhOKuQ2qIdiWkNagyOqigq7tO0XpL7X9732oL4++A3qf56d8/7/PPcG2TtQ0/WpcwwQmvvZ6KwoOJlQTEoKW6DorzAGp5c8y0Xahv/EVl7GTTnPf0+J43iPlyYSNLvBSeNkjn072kwLdYIlxptQQFVgDUWeGBq7PA+Uzta6husIZAy6SZkjXZTAZTaBa3R4T9Jar3jrVE+aLKPizGvsTXWgnKBB7DGAg+EITFR7Rhzxkg8htLfDK29DRJSdvMtzhrFsoRIop1xAVgzxmZHbMtijdAaaEEphWngwUCBB6ZYlPeZ2vtT2/FVs3QTKAa3Q1rztanQ2n4iSfkeH6C17Ihtwht8rNFPQQEFRUutl1JHPGW3S521i3YU8kbgpZZmIEHE3EfWHFvzE0k2pCBr2RGbfoYRWvvUEi6eCNEzbhWiRIEdLo+iNSrtom8Y/6Pzt76DrF15WTwkCYVrzb0pH5GkIrT2mlrx1wJsjf8UFAceFHClBaObE5eoOPxNATdDi8WbkLU7Y0Mfqpud46WBFFyRSt0yeWlG44cCWbUAHmtH2bVjLvAA1kDgAWnCezvDW/ON5KKWedha2uLQ86jRLWTNQmMR6YksZC221p//xhsjtHZ3DW4Fu18H1tiEh5w3bI1iHxT6m1/xElrbpedm21LhWltLZJmUgqwlt82qBXiDNsV7jDahDBR4KNN4tu2mgjXrRBKMwWz6aw3FHYyN95E1R6NCRJJlyJq/HFTqZoTWPo0BP0Uu8BCTP3KvzSkuBhRfyG2DorPS8QbtxAdZcK25D5YikjTolslb85eDhbHYGr0LCqkgCtd0csG8YXCiuBhQMjTBw1dmE7J2pWED/gzZdIVJY6QuJbK0+RzaGRgpHzR0YuM1tPZ+pR7HDlzgQWGBhwQF5ticYmuht6jGfYTW3qLTaustZM0ymciy8x231Cix1YjujREWgyfrCER7HRQDFnjIULlKnJNJ46xNDlLsM0FrN2qXrVy5fPny+X/zN/BNRdYSxhNZdCnIWvJ8sk5kTUcoOPBggMBDhtKvnCJrIZfeO5hNsBhc2dzoN+3/0NrP1hFwhzpGlCCS1BiaCay97qGfaYQb9Cr7VIEPPBgg8JAiIk5oLWN80PBVOrJGeXmbcSsLKg1YczsKEFl6fwbWjDP7vsbW7g0gmF4KgYckJeNwCaXWxgUNXzFpqi4aMGsMSyciy7R3fDGgUGnI2vuZdQkABR4UFnhIk6+FkxUDobXy7e8ga/gmEJZGSVhAZCmWzC01BrfUnmwgGA1baswaCzzk2W49I7C2LeAJLN6UB9YcY4kshVtmSli7W5Bgqj0E1tiEhzwGm8BafEBzMFHZmvIGveTeU5fI0jcZWcMb9G4bYZiO+ikWeMhTP05gzcr2k36FyQ+QJmHtUkIxIsuid+qtsYkYlYHHYxZ4SFLVhooBxb44sAWVLAbYmmUxkUXzWbU1NhEjH3jIn2toqVHidX9b0BHpuTrW2FpbT2QpOvzLSSANWQMTMTjw8IMCj/x6lZOPg53Qmv8tYqwFzRNrjkbliSwTklUWg6NsIgYEHsAaCDy0u+tVVbNp+whLaOv8rAXNI2snjlQi8oMLKq2xiRiOHWCtwcCj2rWka7sr6PIrBEb0IVdgzdw/uAXNfTGgJHQmshT4cFLdscYmYmDgwQgTeFSgd4HOu1z1StcItz37sM6Ae/Cow1rQPLNmiSGyFBrzRZW1q6uEJ3uZRyoDj6Kj/OO4Sd52HeoUJRB9dAsrjokovs2Fsjto2oLmSQml0OE/aeYnq7LGJmI4IrnWnYICj4Ke41ncdHlazYvQdAk+h8uXGlihBcuJeGv26JAWNPfHGsU9S77Ul05Rc6yxSxk8k5A1FHhUC7zimOTyJg6J6lMyevHA6bo6uukR0ROjBtsSnfAzg+wKqs/229qXV9YoB8sQWbSZJ1VYYxMxOPBghAk8JrpC7rTcTPK4vDZb3G9stkT27nRszbyfZFEn3pSH1iyDiCyVe3xRsUHZRAwOPBjiwKPsXnzrDACtpS/XBnwKmlfFgJIwmUizIVnZ2lXQgkoHHtqux3Nlzd6ffQqaDpYa48pfQuS5oTXHPiJNZIqyNdCCKgYeD6qIpuWVh7+RtIyRhQNaUGztSvM/NGzo/9NPM8ZB9wlgTWb4j41EnlQ61nALigMPPyjwqOLlrEkstdNWHRvE9WFrd8bla5AvHI1v8dYoDg2Rn8j6omSNtaAw8ADWUOCx2pObDWo3BA7iYmtpA5UO8VkOZM3SKScHm5K1GaWIkEorr3PWYOBRot3jXFizTwwcxIXW2Ky8mLWpnDRKwgEiTa1YhWPtyQQipslllYFHgaRcFAP7tqKsF1luMsESekM589loQdbA8J8iZTIVrL2pQ8RMUxt41L4mZw1Lo/QXbdB45Ta8egKy5t7TIIetqNja6y0wgMKBhx8UeFTJcQk9bZ9XKPAuaLpgg/LXosC0h5uzRqHDf9KsSw57rD2tRgDsPXXAGgs8cl8MLvqshuC7oCZsLa2/itLXyIGsWTYSaaqlhLPGBnERxR+BYgADj1Y57AzsI5uAiRhg7U5BAgDlAKy19USaAl/CWXsziTBw4MEQBx6ldueohL6yluwS/I/NMAUQKE3Vkd7xPpBGU/CyOTjYMsXHGk6+5QOP6q4clNAM+5zQE6eDcIOq2mWdU3lrlE2lcpKxAWvgsQPQBnWhKPCI/OE5L1cMzljt9SLZ+mFxJLR2lhvFheiPuJG1hKVEmlopYIOCxw70+mFUDNCER4nIPruveR6r7qde2Zf3acwX705mE7b2dru6iKeRA1qbTKSp1O2kyBp7CyKk3PNjyBqe8NAu6tXd60pS3qDOV7ZXUdFwRH9shsBaWm2iirmpyBqNPeTp/TU2iKd+7r15+jWShKPq14d/uBzIgwcPfgpKSKXIklGHvF7PTdFdPSd93XxcizkGTWX8W/pmDiA+i7S0NHNDlbnFxu8WhIPIU5x+NWut35SmRERE1KfUrFlzwMKFi8Jn6sWq1c+mZiCLi4sfmbRNo3fsbXHT4/Ek3Xz2LMvWqd/frmq1vmqxd3t040pERMGFRQQ0VTvpGDO1o/9vdOwYExMz9TcxHTfWJ/8BhfTFBhpK9qk3mn6P76FDy5cPHjIyas7EKkt02qLk3/ALfyCw1l8ukpQAAAAASUVORK5CYII="/>
</defs>
</svg></div>
                        <div class="payment__item"><img src="{{asset('images/mastercard.svg')}}" height="44" alt=""></div>
                    </div>
                </div>
                <div class="reservation__complete-md flex-wrap justify-content-center">
                    <div class="reservation__complete">Completing this booking you agree to the booking conditions, general terms, and privacy policy. </div>
                    <button class="reservation__button">Pay and complete booking</button>
                </div>
            </div>
        </form>
    </div>
</div>
        `
    });
function validate(evt) {
  let theEvent = evt;
    let key;
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
      key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  let regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}
$(document).ready(function(){
    $('#phone').on("keypress",function(evt){
        validate(evt);
    });
    $("#country").countrySelect({
        preferredCountries: ['uz', 'gb', 'us'],
        responsiveDropdown: true
    });
});
</script>
@endsection
