@extends('layouts.frontend')

@section('content')
    @include('front_partials.index_nav')
    <main role="main">
        <div class="container">
            <div class="row">
                <div class="main__slider main__city">
                    <div class="city">
                        <div class="title">Tashkent</div>
                        <div class="count">387 objects</div>
                        <img src="{{asset('images/city-1.png')}}" alt="city">
                    </div>
                    <div class="city">
                        <div class="title">Tashkent</div>
                        <div class="count">387 objects</div>
                        <img src="{{asset('images/city-1.png')}}" alt="city">
                    </div>
                    <div class="city">
                        <div class="title">Tashkent</div>
                        <div class="count">387 objects</div>
                        <img src="{{asset('images/city-1.png')}}" alt="city">
                    </div>
                    <div class="city">
                        <div class="title">Tashkent</div>
                        <div class="count">387 objects</div>
                        <img src="{{asset('images/city-1.png')}}" alt="city">
                    </div>
                </div>
                <div class="main__offer col-12">
                    <div class="main__offer-block text-center">
                        <i class="fa fa-star"></i>
                        <span>Best offers today</span>
                    </div>
                </div>
            </div>
        </div>
    <div class="other">
        <div class="container">
            <div class="row">
                <div class="other__block d-flex align-items-center justify-content-between flex-wrap w-100">
                    @foreach($hotels as $hotel)
                        @include('front_partials.hotel-card')
                    @endforeach
                </div>
                <div class="more">
                    <button>See all</button>
                </div>
            </div>
        </div>
    </div>
    </main>
    @include('front_partials.add-hotel')
@endsection

@section('script')
<script src="{{asset('js/slick.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.main__slider').slick({
            infinite: true,
            slidesToShow: 3,
            swipeToSlide: true,
            variableWidth: true,
            dots: false,
            slidesToScroll: 1,
            arrows: true,
            prevArrow:'<svg width="40" class="slick-prev" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg"> <g opacity="0.5"> <path d="M25 30L15 20L25 10" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/> </g> </svg> ',
            nextArrow:'<svg width="40" class="slick-next" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg"> <g opacity="0.5"> <path d="M15 30L25 20L15 10" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/> </g> </svg> '
        });
        let today = new Date();
          if ($(window).width() < 991) {
              $('.daterange').daterangepicker({
                    autoUpdateInput: false,
                    constrainInput:false,
                    singleDatePicker: false,
                    linkedCalendars: false,
                    minDate: today,
                    applyButtonClasses: 'add-list-btn',
                    autoApply: true,
                    locale: {
                        "format": "MMMM D",
                        "separator": " — ",
                        "firstDay": 1,
                    },
                }, function(start, end, label) {
                   let params = {
                        from: moment(start).format('D-M-Y'),
                        to: moment(end).format('D-M-Y')
                    };
                });
            }
            else {
              $('.daterange').daterangepicker({
                  opens: 'center',
                  autoUpdateInput: false,
                  constrainInput: true,
                  singleDatePicker: false,
                  minDate: today,
                  maxSpan: {
                      "days": 30
                  },
                  locale: {
                      "format": "MMMM D",
                      "separator": " — ",
                      "fromLabel": "From",
                      "toLabel": "To",
                      "customRangeLabel": "Custom",
                      "weekLabel": "W",
                      "daysOfWeek": [
                          "Su",
                          "Mo",
                          "Tu",
                          "We",
                          "Th",
                          "Fr",
                          "Sa"
                      ],
                      "monthNames": [
                          "January",
                          "February",
                          "March",
                          "April",
                          "May",
                          "June",
                          "July",
                          "August",
                          "September",
                          "October",
                          "November",
                          "December"
                      ],
                      "firstDay": 1
                  },
                  autoApply: true,
                  startDate: moment().startOf('day'),
              }, function (start, end, label) {
                  let params = {
                      from: moment(start).format('MMMM D'),
                      to: moment(end).format('MMMM D')
                  };
                  $('.daterange').val(params.from + ' — ' + params.to)
              });
          }
    })
</script>
@endsection
