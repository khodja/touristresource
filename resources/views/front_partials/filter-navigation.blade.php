<a href="#filter1" class="active">
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M5.83332 18.3333H3.33332C2.8913 18.3333 2.46737 18.1577 2.15481 17.8452C1.84225 17.5326 1.66666 17.1087 1.66666 16.6667V10.8333C1.66666 10.3913 1.84225 9.96737 2.15481 9.65481C2.46737 9.34225 2.8913 9.16666 3.33332 9.16666H5.83332M11.6667 7.49999V4.16666C11.6667 3.50362 11.4033 2.86773 10.9344 2.39889C10.4656 1.93005 9.8297 1.66666 9.16666 1.66666L5.83332 9.16666V18.3333H15.2333C15.6353 18.3379 16.0253 18.197 16.3316 17.9367C16.6379 17.6763 16.8397 17.3141 16.9 16.9167L18.05 9.41666C18.0862 9.17779 18.0701 8.93389 18.0028 8.70187C17.9354 8.46984 17.8184 8.25524 17.6599 8.07292C17.5013 7.8906 17.3051 7.74493 17.0846 7.646C16.8642 7.54708 16.6249 7.49725 16.3833 7.49999H11.6667Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
    <span class="text">Recommended</span>
</a>
<a href="#filter2">
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M10 1.66666L12.575 6.88332L18.3334 7.72499L14.1667 11.7833L15.15 17.5167L10 14.8083L4.85002 17.5167L5.83335 11.7833L1.66669 7.72499L7.42502 6.88332L10 1.66666Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
    <span class="text">Rating</span>
</a>
<a href="#filter3">
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M10 4.16666V15.8333" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M15.8334 10L10 15.8333L4.16669 10" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
    <span class="text">Price — down</span>
</a>
<a href="#filter3">

    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M15.8333 4.16666L4.16663 15.8333" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M5.41671 7.50001C6.5673 7.50001 7.50004 6.56727 7.50004 5.41668C7.50004 4.26608 6.5673 3.33334 5.41671 3.33334C4.26611 3.33334 3.33337 4.26608 3.33337 5.41668C3.33337 6.56727 4.26611 7.50001 5.41671 7.50001Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M14.5833 16.6667C15.7339 16.6667 16.6667 15.7339 16.6667 14.5833C16.6667 13.4327 15.7339 12.5 14.5833 12.5C13.4327 12.5 12.5 13.4327 12.5 14.5833C12.5 15.7339 13.4327 16.6667 14.5833 16.6667Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
    <span class="text">Special offers</span>
</a>
<a href="#filter3">
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M17.5 8.33334C17.5 14.1667 10 19.1667 10 19.1667C10 19.1667 2.5 14.1667 2.5 8.33334C2.5 6.34422 3.29018 4.43656 4.6967 3.03004C6.10322 1.62352 8.01088 0.833344 10 0.833344C11.9891 0.833344 13.8968 1.62352 15.3033 3.03004C16.7098 4.43656 17.5 6.34422 17.5 8.33334Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M10 10.8333C11.3807 10.8333 12.5 9.71405 12.5 8.33334C12.5 6.95263 11.3807 5.83334 10 5.83334C8.61929 5.83334 7.5 6.95263 7.5 8.33334C7.5 9.71405 8.61929 10.8333 10 10.8333Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
    <span class="text">Great location</span>
</a>
