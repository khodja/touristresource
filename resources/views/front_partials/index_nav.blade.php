<header class="header">
    <nav class="nav-main">
        <div class="container">
            <div class="row d-flex justify-content-between flex-wrap">
                <div class="col-sm-7 d-flex align-items-center">
                    <div class="nav__logo">
                        <img src="{{asset('images/logo.svg')}}" alt="logo" />
                    </div>
                    <ul class="nav__links d-flex justify-content-between">
                        <li class="nav__links-item">
                            <a href="#" class="white-color"><i class="fa fa-hotel"></i> Hotels</a>
                        </li>
                        <li class="nav__links-item">
                            <a href="#" class="white-color"><i class="fa fa-car"></i> Transfers & Cars</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-5 d-flex justify-content-end align-items-center">
                <div class="nav__lang">
                    <div class="languages languages-main">
                        <a href="#" class="item-transp">En</a>
                        <a href="#" class="item-transp">Ru</a>
                    </div>
                </div>
                <div class="nav__add "><a href="#" class="white-color">Add hotel</a></div>
                <div class="nav__login"><a href="#">Sign in</a></div>
            </div>
            </div>
        </div>
    </nav>
    <nav class="nav-mobile d-lg-none">
        <div class="container h-100">
            <div class="row d-flex justify-content-between align-items-center h-100">
                <div class="logo">
                    <img src="{{asset('images/mobile-logo.svg')}}" alt="logo" />
                </div>
                <div class="nav-mobile-blocks d-flex align-items-center justify-content-between">
                <div class="block">
                   <a href="#" class="links active"><i class="fa fa-hotel"></i></a>
                </div>
                <div class="block">
                    <a href="#" class="links"><i class="fa fa-car"></i></a>
                </div>
                <div class="block">
                    <a href="#" class="links"><i class="fa fa-search"></i></a>
                </div>
                <div class="block">
                    <button class="hamburger links"><img src="{{asset('images/hamburger.svg')}}" alt="logo" /></button>
                </div>
            </div>
            </div>
        </div>
    </nav>
    <div class="main__content">
        <div class="container">
            <form action="{{action('PageController@search')}}" class="row search-form" autocomplete="off" method="GET">
                <div class="col-md-12 text-center main__content-travel">
                    <div class="hidden-block">
                        <img src="{{asset('images/travel.svg')}}" alt="logo" />
                    </div>
                    <div class="d-lg-none">
                        <img src="{{asset('images/travel-mobile.svg')}}" alt="logo" />
                    </div>
                </div>
                    <div class="col-md-12 high-index">
                        <keyword></keyword>
                    </div>
                <div class="col-lg-4">
                    <div class="main__content-box fixed-padding d-flex align-items-center">
                        <label for="keyword" class="d-inline-flex justify-content-center align-items-center"><i class="search__icon" data-feather="search"></i></label>
                        <input type="text" required="required" placeholder="Сity or hotel" name="keyword" id="keyword" class="keyword" />
                    </div>
                </div>
                <div class="col-lg-4 date-parent">
                    <div class="main__content-box fixed-padding d-flex justify-content-between align-items-center">
                        <div class="d-flex">
                        <label for="calendar" class="d-inline-flex justify-content-center align-items-center"><i class="search__icon" data-feather="calendar"></i></label>
                        <input value="Check-in — Check out" id="daterange" readonly="readonly" class="daterange date-input" name="date" type="text" />
                        </div>
                        <div class="d-flex">
                            <span class="arrow d-flex align-items-center">
                                <img src="{{asset('images/arrow-index.svg')}}" alt="logo" />
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="main__content-box fixed-padding d-flex align-items-center justify-content-between">
                        <div class="d-flex">
                            <label for="adults" class="d-inline-flex justify-content-center align-items-center"><i class="search__icon" data-feather="user"></i></label>
                            <div class="adults adult-changer"><span class="adult-count">1</span> adults, <span class="children-count">0</span> child, <span class="room-count">1</span> rooms</div>
                        </div>
                        <div class="d-flex">
                            <input value="1,0,1" type="hidden" class="adult-changer" name="information" />
                            <span class="arrow d-flex align-items-center"><img src="{{asset('images/arrow-index.svg')}}" alt="logo" /></span>
                        </div>
                    </div>
                    <div class="amount high-index">
                        <div class="amount__box d-flex justify-content-between">
                            <div class="left d-flex align-items-center">
                                <div class="icon d-flex align-items-center justify-content-center"><img src="{{asset('images/adult.svg')}}" alt="logo" /></div>
                                <div class="title d-flex align-items-center">Adults</div>
                            </div>
                            <div class="right d-flex justify-content-between align-items-center">
                                <div class="plus amount__box-button d-flex justify-content-center align-items-center"><img src="{{asset('images/index-plus.svg')}}" class="pointer" alt="plus" /></div>
                                <div class="counter adult-count d-flex justify-content-center align-items-center" data-count="adult-count">1</div>
                                <div class="minus amount__box-button d-flex justify-content-center align-items-center"><img src="{{asset('images/index-minus.svg')}}" alt="minus" /></div>
                            </div>
                        </div>
                        <div class="amount__box d-flex justify-content-between">
                            <div class="left d-flex align-items-center">
                                <div class="icon d-flex align-items-center justify-content-center"><img src="{{asset('images/children.svg')}}" alt="children" /></div>
                                <div class="title d-flex align-items-center">Children</div>
                            </div>
                            <div class="right d-flex justify-content-between align-items-center">
                                <div class="plus amount__box-button d-flex justify-content-center align-items-center"><img src="{{asset('images/index-plus.svg')}}" class="pointer" alt="plus" /></div>
                                <div class="counter children-count d-flex justify-content-center align-items-center" data-count="children-count">0</div>
                                <div class="minus amount__box-button d-flex justify-content-center align-items-center"><img src="{{asset('images/index-minus.svg')}}" alt="minus" /></div>
                            </div>
                        </div>
                        <div class="amount__box d-flex justify-content-between">
                            <div class="left d-flex align-items-center">
                                <div class="icon d-flex align-items-center justify-content-center"><img src="{{asset('images/rooms.svg')}}" alt="rooms" /></div>
                                <div class="title d-flex align-items-center">Rooms</div>
                            </div>
                            <div class="right d-flex justify-content-between align-items-center">
                                <div class="plus amount__box-button d-flex justify-content-center align-items-center"><img src="{{asset('images/index-plus.svg')}}" class="pointer" alt="plus" /></div>
                                <div class="counter room-count d-flex justify-content-center align-items-center" data-count="room-count">1</div>
                                <div class="minus amount__box-button d-flex justify-content-center align-items-center"><img src="{{asset('images/index-minus.svg')}}" alt="minus" /></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <button class="main__content-btn">
                        Show prices
                    </button>
                    <div class="main__content-text">Book online a hotel, apartment or hostel</div>
                </div>
            </form>
        </div>
    </div>
</header>
