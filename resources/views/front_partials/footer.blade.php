<footer class="footer">
    <div class="container">
        <div class="row d-flex flex-wrap justify-content-between">
            <div class="col-lg-3">
                <img src="{{asset('images/logo.svg')}}" alt="logo" class="footer__logo">
            </div>
            <div class="col-lg-2 col-6">
                <div class="footer__title red">Hotels</div>
                <ul>
                    <li><a href="#">Without breakfast</a></li>
                    <li><a href="#">With breakfast</a></li>
                    <li><a href="#">All inclusive</a></li>
                    <li><a href="#">Suites</a></li>
                    <li><a href="#">Apartments</a></li>
                    <li><a href="#">Houses</a></li>
                    <li><a href="#">Hostels</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-6">
                <div class="footer__title gold">Transfers & Cars</div>
                <ul>
                    <li><a href="#">Self drive</a></li>
                    <li><a href="#">Chauffeur</a></li>
                    <li><a href="#">Transfers</a></li>
                </ul>
            </div>
            <div class="col-lg-3 footer__support">
                <div class="footer__title blue">Support</div>
                <div class="phone">998 97 700 00 00</div>
                <div class="phone">998 98 800 00 00</div>
                <div class="email"><a href="">support@touristrcs.com</a></div>
                <div class="help"><a href="">Help center</a></div>
            </div>
            <div class="col-lg-2 footer__about">
                <div class="footer__title">Blog</div>
                <ul>
                    <li><a href="#">About TRC</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="middle container text-center d-flex align-items-center justify-content-center flex-wrap">
            <a href="#" class="login d-flex align-items-center justify-content-center"><svg width="20" height="20"
                                           viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M12.5 2.5H15.8333C16.2754 2.5 16.6993 2.67559 17.0118 2.98816C17.3244 3.30072 17.5 3.72464 17.5 4.16667V15.8333C17.5 16.2754 17.3244 16.6993 17.0118 17.0118C16.6993 17.3244 16.2754 17.5 15.8333 17.5H12.5" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M8.3335 14.1663L12.5002 9.99967L8.3335 5.83301" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M12.5 10H2.5" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg> Login to extranet</a>
            <a href="#" class="register">Register object</a>
    </div>
    <div class="container">
        <div class="row d-flex justify-content-between align-items-center flex-wrap">
            <div class="col-lg-6 d-flex align-items-center footer__terms">
                <div class="copyright">Copyright © Tourist Resources</div>
                <div class="terms"><a href="">Terms & Conditions</a></div>
            </div>
            <div class="col-lg-6 d-flex align-items-center justify-content-end footer__payment">
                <div class="accept">Accepted for payment:</div>
                <div class="cards d-flex align-items-center">
                    <svg width="46" height="26" viewBox="0 0 46 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M33.1471 0C40.2446 0 46 5.81989 46 13C46 20.1801 40.2446 26 33.1471 26C29.0219 26 25.3499 24.0349 23 20.9793C24.6966 18.7761 25.7059 16.0079 25.7059 13C25.7059 9.99221 24.6966 7.2239 23 5.02074C25.3499 1.96505 29.0219 0 33.1471 0Z" fill="#F79F1A"/>
                    <path d="M22.9998 5.02051C24.6964 7.22367 25.7057 9.99198 25.7057 12.9998C25.7057 16.0076 24.6964 18.7759 22.9998 20.979C21.3032 18.7759 20.2939 16.0077 20.2939 12.9998C20.2939 9.99198 21.3032 7.22367 22.9998 5.02051Z" fill="#FF5F01"/>
                    <path d="M12.8529 0C16.978 0 20.6499 1.96505 23 5.02074C21.3034 7.2239 20.2941 9.99221 20.2941 13C20.2941 16.0078 21.3034 18.7761 23 20.9793C20.6499 24.0349 16.978 26 12.8529 26C5.75406 26 0 20.1801 0 13C0 5.81989 5.75406 0 12.8529 0Z" fill="#EA001B"/>
                    </svg>
                    <svg width="46" height="15" viewBox="0 0 46 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M18.5899 0.259361L16.2487 14.7506H19.9944L22.3359 0.259361H18.5899ZM13.0972 0.259361L9.5259 10.2263L9.10346 8.08007L9.10376 8.08077L9.01067 7.59973C8.57846 6.66158 7.57743 4.87851 5.68722 3.35681C5.12839 2.90703 4.56507 2.52316 4.02051 2.19527L7.26624 14.7506H11.169L17.1287 0.259361H13.0972ZM27.7161 4.28241C27.7161 2.64405 31.3641 2.85457 32.9671 3.7442L33.5016 0.63187C33.5016 0.63187 31.8519 0 30.1324 0C28.2734 0 23.8592 0.818779 23.8592 4.79721C23.8592 8.54141 29.0402 8.58794 29.0402 10.5537C29.0402 12.5194 24.3933 12.1681 22.8598 10.9281L22.3025 14.1812C22.3025 14.1812 23.975 15 26.5311 15C29.0869 15 32.9437 13.6664 32.9437 10.0389C32.9437 6.27128 27.7161 5.92037 27.7161 4.28241ZM43.0014 0.259361H39.9897C38.5991 0.259361 38.2604 1.33931 38.2604 1.33931L32.6743 14.7506H36.5786L37.3596 12.5982H42.1216L42.5609 14.7506H46.0003L43.0014 0.259361ZM38.439 9.62447L40.4074 4.20152L41.5147 9.62447H38.439Z" fill="white"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M7.89437 1.62426C7.89437 1.62426 7.73932 0.316406 6.08408 0.316406H0.0704399L0 0.5622C0 0.5622 2.89063 1.15568 5.66384 3.37918C8.31402 5.50429 9.17855 8.15336 9.17855 8.15336L7.89437 1.62426Z" fill="#F6AC1D"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</footer>
