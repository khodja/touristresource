<header class="second">
    <div class="top-block">
        <div class="top-line">
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="left-block">
                    <a href="{{ action('HomeController@index')  }}">
                        <img src="{{ asset('backend/images/logo.svg') }}" />
                    </a>
                    <span class="description">Hotel Management System</span>
                </div>
                <div class="right-block">
                    <span class="btn badge-default tag-rounded" style="color: #00236D;font-weight: normal;">Помощь</span>
                    @include('partials.dropdown')
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-block menu">
        <div class="container">
            <div class="row">
            <div class="jv_menu_list">
                <a href="{{ action('HomeController@index') }}" class="{{ request()->is('dashboard') ? 'active' : '' }}">
                    <i class="fe fe-home"></i>
                    <span>Все отели</span>
                </a>
                    @if(Auth::user()->hasRole('manager'))
                    <a href="{{ (isset($id) ? action('BookingController@index',$id) : action('HomeController@index') ) }}"><i class="fa fa-calendar"></i><span>Брони</span></a>
                    <a href="{{ (isset($id) ? action('RoomController@index',$id) : action('HomeController@index') ) }}"><i class="fa fa-key"></i><span>Номера</span></a>
                    <a href="{{ (isset($id) ? action('PartnerController@index',$id) : action('HomeController@index') ) }}"><i class="fa fa-briefcase"></i><span>Партнеры</span></a>
                    <a href="{{ (isset($id) ? action('PriceController@index',$id) : action('HomeController@index') ) }}"><i class="fa fa-dollar"></i><span>Цены</span></a>
                    <a href="{{ (isset($id) ? action('SaleController@index',$id) : action('HomeController@index') ) }}"><i class="fa fa-percent"></i><span>Скидки</span></a>
                    @endif
                    @if(Auth::user()->hasRole('admin'))
                        <a href="{{ action('TypeController@index') }}" class="{{ request()->is('*type*') ? 'active' : '' }}"><i class="fe fe-package"></i> Тип обьекта</a>
                        <a href="{{ action('BedController@index') }}" class="{{ request()->is('*bed*') ? 'active' : '' }}"><i class="fe fe-home"></i> Кровать</a>
                        <a href="{{ action('ServiceController@index') }}" class="{{ request()->is('*dashboard/service*') ? 'active' : '' }}"><i class="fe fe-server"></i> Сервис</a>
                        <a href="{{ action('RoomServiceController@index') }}" class="{{ request()->is('*room/service*') ? 'active' : '' }}"><i class="fe fe-fast-forward"></i> Сервис - Номеров</a>
                        <a href="{{ action('CountryController@index') }}" class="{{ request()->is('*country*') ? 'active' : '' }} "><i class="fe fe-flag"></i> Страна</a>
                        <a href="{{ action('PublicationController@index') }}" class="{{ request()->is('*publication*') ? 'active' : '' }} "><i class="fe fe-dollar-sign"></i> Публикации</a>
{{--                        <a href="{{ action('CategoryController@index') }}" class="{{ request()->is('*categories*') ? 'active' : '' }} "><i class="fe fe-grid"></i> Категории</a>--}}
                        <a href="{{ action('ManagerController@index') }}" class="{{ request()->is('*manager*') ? 'active' : '' }} "><i class="fe fe-user"></i> Менеджеры</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>
