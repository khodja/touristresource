	<header>
    <div class="top-block">
        <div class="top-line">
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="left-block">
                    <a href="{{ action('HomeController@index')  }}">
                        <img src="{{ asset('backend/images/logo.svg') }}" />
                    </a>
                    <span class="description">Hotel Management System</span>
                </div>
            </div>
        </div>
    </div>
        <div class="bottom-block">
            <div class="container">
                <div class="row jv_register_menu">
                    <a href="#" class="{{ request()->is('*register')? 'active' : 'disabled' }}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg><span>Личные данные</span></a>
                    <a href="#" class="{{ request()->is('*dashboard/hotel/create') ? 'active' : 'disabled' }}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg><span>Информация об объекте</span></a>
                    <a href="#" class="{{ request()->is('*dashboard/hotel/rooms*') ? 'active' : 'disabled' }}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-key"><path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path></svg><span>Добавление номеров</span></a>
                </div>
            </div>
        </div>
	</header>
