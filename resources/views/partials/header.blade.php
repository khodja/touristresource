<header class="second">
    <div class="top-block">
        <div class="top-line">
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="left-block">
                    <a href="{{ action('HomeController@index')  }}">
                        <img src="{{ asset('backend/images/logo.svg') }}" />
                    </a>
                    <span class="description">Hotel Management System</span>
                </div>
                <div class="right-block">
                    <a href="#" class="help">Помощь</a>
                    @include('partials.dropdown')
                </div>
            </div>
        </div>
    </div>
<div class="bottom-block menu">
    <div class="container">
        <div class="row">
            <div class="dropdown">
                <a href="#" class="name" data-toggle="dropdown">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                    <span>{{ ( !$hotels  ?  'Управление обьектами' : $hotels->where('id',$id)->first()->name) }}</span>
                    <img src="{{ asset('backend/images/dropdown.png')}}" alt="" class="arrow">
                </a>
                <ul class="dropdown-menu p-0 dropdown-menu-left dropdown-menu-arrow dropdown-block">
                    @foreach($hotels as $key => $hotel)
                        @if($hotel->id != (isset($id) ? $id : 0) )
                            <li><a href="{{ action('BookingController@index',$hotel->id)  }}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>{{ $hotel->name }}</a></li>
                        @endif
                    @endforeach
                    <li><a href="/dashboard"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>Управление объектами</a></li>
                </ul>
            </div>
            <div class="jv_menu_list">
                <img src="{{ asset('backend/images/dropdown.png')}}" alt="" class="arrow">
                <a href="{{ (isset($id) ? action('BookingController@index',$id) : action('HomeController@index'))}}" class="{{ request()->is('*booking*') ? 'active' : '' }}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg><span>Брони</span> </a>
                <a href="{{ (isset($id) ? action('RoomController@index',$id) : action('HomeController@index'))}}" class="{{ request()->is('*room*') ? 'active' : '' }}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-key"><path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path></svg><span>Номера</span></a>
                <a href="{{ (isset($id) ? action('PartnerController@index',$id) : action('HomeController@index'))}}" class="{{ request()->is('*partner*') ? 'active' : '' }}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-briefcase"><rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect><path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path></svg><span>Партнеры</span></a>
                <a href="{{ (isset($id) ? action('PriceController@index',$id) : action('HomeController@index'))}}" class="{{ request()->is('*price*') ? 'active' : '' }}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg><span>Цены</span></a>
                <a href="{{ (isset($id) ? action('SaleController@index',$id) : action('HomeController@index'))}}" class="{{ request()->is('*sale*') ? 'active' : '' }}" ><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-percent"><line x1="19" y1="5" x2="5" y2="19"></line><circle cx="6.5" cy="6.5" r="2.5"></circle><circle cx="17.5" cy="17.5" r="2.5"></circle></svg><span>Скидки</span></a>
            </div>
        </div>
    </div>
</div>
</header>
@include('partials.message')
