@extends('layouts.backend')

@section('content')
@include(Auth::user()->hasRole('admin') ? 'partials.admin_header':'partials.header')
<div class="my-3 my-md-5">
    <div class="container">
            <div class="card">
                <div class="card-header">Добавить Город</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('CountryController@store') }}" method="POST" class="reg-form" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label for="name-en">Название (EN)</label>
                            <input name="name_en" required="required" type="text" class="form-control" id="name-en" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="name-ru">Название (RU)</label>
                            <input name="name_ru" required="required" type="text" class="form-control" id="name-ru" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="select-countries">Флаг</label>
                            <select name="flag"  id="select-countries" class="form-control custom-select">
                            <option data-data='{"image": "/backend/images/flags/ad.svg"}' value="ad">ad<i class="flag flag-ad"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ae.svg"}' value="ae">ae<i class="flag flag-ae"></i></option>
                            <option data-data='{"image": "/backend/images/flags/af.svg"}' value="af">af<i class="flag flag-af"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ag.svg"}' value="ag">ag<i class="flag flag-ag"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ai.svg"}' value="ai">ai<i class="flag flag-ai"></i></option>
                            <option data-data='{"image": "/backend/images/flags/al.svg"}' value="al">al<i class="flag flag-al"></i></option>
                            <option data-data='{"image": "/backend/images/flags/am.svg"}' value="am">am<i class="flag flag-am"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ao.svg"}' value="ao">ao<i class="flag flag-ao"></i></option>
                            <option data-data='{"image": "/backend/images/flags/aq.svg"}' value="aq">aq<i class="flag flag-aq"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ar.svg"}' value="ar">ar<i class="flag flag-ar"></i></option>
                            <option data-data='{"image": "/backend/images/flags/as.svg"}' value="as">as<i class="flag flag-as"></i></option>
                            <option data-data='{"image": "/backend/images/flags/at.svg"}' value="at">at<i class="flag flag-at"></i></option>
                            <option data-data='{"image": "/backend/images/flags/au.svg"}' value="au">au<i class="flag flag-au"></i></option>
                            <option data-data='{"image": "/backend/images/flags/aw.svg"}' value="aw">aw<i class="flag flag-aw"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ax.svg"}' value="ax">ax<i class="flag flag-ax"></i></option>
                            <option data-data='{"image": "/backend/images/flags/az.svg"}' value="az">az<i class="flag flag-az"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ba.svg"}' value="ba">ba<i class="flag flag-ba"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bb.svg"}' value="bb">bb<i class="flag flag-bb"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bd.svg"}' value="bd">bd<i class="flag flag-bd"></i></option>
                            <option data-data='{"image": "/backend/images/flags/be.svg"}' value="be">be<i class="flag flag-be"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bf.svg"}' value="bf">bf<i class="flag flag-bf"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bg.svg"}' value="bg">bg<i class="flag flag-bg"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bh.svg"}' value="bh">bh<i class="flag flag-bh"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bi.svg"}' value="bi">bi<i class="flag flag-bi"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bj.svg"}' value="bj">bj<i class="flag flag-bj"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bl.svg"}' value="bl">bl<i class="flag flag-bl"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bm.svg"}' value="bm">bm<i class="flag flag-bm"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bn.svg"}' value="bn">bn<i class="flag flag-bn"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bo.svg"}' value="bo">bo<i class="flag flag-bo"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bq.svg"}' value="bq">bq<i class="flag flag-bq"></i></option>
                            <option data-data='{"image": "/backend/images/flags/br.svg"}' value="br">br<i class="flag flag-br"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bs.svg"}' value="bs">bs<i class="flag flag-bs"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bt.svg"}' value="bt">bt<i class="flag flag-bt"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bv.svg"}' value="bv">bv<i class="flag flag-bv"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bw.svg"}' value="bw">bw<i class="flag flag-bw"></i></option>
                            <option data-data='{"image": "/backend/images/flags/by.svg"}' value="by">by<i class="flag flag-by"></i></option>
                            <option data-data='{"image": "/backend/images/flags/bz.svg"}' value="bz">bz<i class="flag flag-bz"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ca.svg"}' value="ca">ca<i class="flag flag-ca"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cc.svg"}' value="cc">cc<i class="flag flag-cc"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cd.svg"}' value="cd">cd<i class="flag flag-cd"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cf.svg"}' value="cf">cf<i class="flag flag-cf"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cg.svg"}' value="cg">cg<i class="flag flag-cg"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ch.svg"}' value="ch">ch<i class="flag flag-ch"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ci.svg"}' value="ci">ci<i class="flag flag-ci"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ck.svg"}' value="ck">ck<i class="flag flag-ck"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cl.svg"}' value="cl">cl<i class="flag flag-cl"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cm.svg"}' value="cm">cm<i class="flag flag-cm"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cn.svg"}' value="cn">cn<i class="flag flag-cn"></i></option>
                            <option data-data='{"image": "/backend/images/flags/co.svg"}' value="co">co<i class="flag flag-co"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cr.svg"}' value="cr">cr<i class="flag flag-cr"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cu.svg"}' value="cu">cu<i class="flag flag-cu"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cv.svg"}' value="cv">cv<i class="flag flag-cv"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cw.svg"}' value="cw">cw<i class="flag flag-cw"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cx.svg"}' value="cx">cx<i class="flag flag-cx"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cy.svg"}' value="cy">cy<i class="flag flag-cy"></i></option>
                            <option data-data='{"image": "/backend/images/flags/cz.svg"}' value="cz">cz<i class="flag flag-cz"></i></option>
                            <option data-data='{"image": "/backend/images/flags/de.svg"}' value="de">de<i class="flag flag-de"></i></option>
                            <option data-data='{"image": "/backend/images/flags/dj.svg"}' value="dj">dj<i class="flag flag-dj"></i></option>
                            <option data-data='{"image": "/backend/images/flags/dk.svg"}' value="dk">dk<i class="flag flag-dk"></i></option>
                            <option data-data='{"image": "/backend/images/flags/dm.svg"}' value="dm">dm<i class="flag flag-dm"></i></option>
                            <option data-data='{"image": "/backend/images/flags/do.svg"}' value="do">do<i class="flag flag-do"></i></option>
                            <option data-data='{"image": "/backend/images/flags/dz.svg"}' value="dz">dz<i class="flag flag-dz"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ec.svg"}' value="ec">ec<i class="flag flag-ec"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ee.svg"}' value="ee">ee<i class="flag flag-ee"></i></option>
                            <option data-data='{"image": "/backend/images/flags/eg.svg"}' value="eg">eg<i class="flag flag-eg"></i></option>
                            <option data-data='{"image": "/backend/images/flags/eh.svg"}' value="eh">eh<i class="flag flag-eh"></i></option>
                            <option data-data='{"image": "/backend/images/flags/er.svg"}' value="er">er<i class="flag flag-er"></i></option>
                            <option data-data='{"image": "/backend/images/flags/es.svg"}' value="es">es<i class="flag flag-es"></i></option>
                            <option data-data='{"image": "/backend/images/flags/et.svg"}' value="et">et<i class="flag flag-et"></i></option>
                            <option data-data='{"image": "/backend/images/flags/eu.svg"}' value="eu">eu<i class="flag flag-eu"></i></option>
                            <option data-data='{"image": "/backend/images/flags/fi.svg"}' value="fi">fi<i class="flag flag-fi"></i></option>
                            <option data-data='{"image": "/backend/images/flags/fj.svg"}' value="fj">fj<i class="flag flag-fj"></i></option>
                            <option data-data='{"image": "/backend/images/flags/fk.svg"}' value="fk">fk<i class="flag flag-fk"></i></option>
                            <option data-data='{"image": "/backend/images/flags/fm.svg"}' value="fm">fm<i class="flag flag-fm"></i></option>
                            <option data-data='{"image": "/backend/images/flags/fo.svg"}' value="fo">fo<i class="flag flag-fo"></i></option>
                            <option data-data='{"image": "/backend/images/flags/fr.svg"}' value="fr">fr<i class="flag flag-fr"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ga.svg"}' value="ga">ga<i class="flag flag-ga"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gb.svg"}' value="gb">gb<i class="flag flag-gb"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gb-eng.svg"}' value="eng">eng<i class="flag flag-gb-eng"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gb-nir.svg"}' value="nir">nir<i class="flag flag-gb-nir"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gb-sct.svg"}' value="sct">sct<i class="flag flag-gb-sct"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gb-wls.svg"}' value="wls">wls<i class="flag flag-gb-wls"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gd.svg"}' value="gd">gd<i class="flag flag-gd"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ge.svg"}' value="ge">ge<i class="flag flag-ge"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gf.svg"}' value="gf">gf<i class="flag flag-gf"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gg.svg"}' value="gg">gg<i class="flag flag-gg"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gh.svg"}' value="gh">gh<i class="flag flag-gh"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gi.svg"}' value="gi">gi<i class="flag flag-gi"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gl.svg"}' value="gl">gl<i class="flag flag-gl"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gm.svg"}' value="gm">gm<i class="flag flag-gm"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gn.svg"}' value="gn">gn<i class="flag flag-gn"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gp.svg"}' value="gp">gp<i class="flag flag-gp"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gq.svg"}' value="gq">gq<i class="flag flag-gq"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gr.svg"}' value="gr">gr<i class="flag flag-gr"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gs.svg"}' value="gs">gs<i class="flag flag-gs"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gt.svg"}' value="gt">gt<i class="flag flag-gt"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gu.svg"}' value="gu">gu<i class="flag flag-gu"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gw.svg"}' value="gw">gw<i class="flag flag-gw"></i></option>
                            <option data-data='{"image": "/backend/images/flags/gy.svg"}' value="gy">gy<i class="flag flag-gy"></i></option>
                            <option data-data='{"image": "/backend/images/flags/hk.svg"}' value="hk">hk<i class="flag flag-hk"></i></option>
                            <option data-data='{"image": "/backend/images/flags/hm.svg"}' value="hm">hm<i class="flag flag-hm"></i></option>
                            <option data-data='{"image": "/backend/images/flags/hn.svg"}' value="hn">hn<i class="flag flag-hn"></i></option>
                            <option data-data='{"image": "/backend/images/flags/hr.svg"}' value="hr">hr<i class="flag flag-hr"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ht.svg"}' value="ht">ht<i class="flag flag-ht"></i></option>
                            <option data-data='{"image": "/backend/images/flags/hu.svg"}' value="hu">hu<i class="flag flag-hu"></i></option>
                            <option data-data='{"image": "/backend/images/flags/id.svg"}' value="id">id<i class="flag flag-id"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ie.svg"}' value="ie">ie<i class="flag flag-ie"></i></option>
                            <option data-data='{"image": "/backend/images/flags/il.svg"}' value="il">il<i class="flag flag-il"></i></option>
                            <option data-data='{"image": "/backend/images/flags/im.svg"}' value="im">im<i class="flag flag-im"></i></option>
                            <option data-data='{"image": "/backend/images/flags/in.svg"}' value="in">in<i class="flag flag-in"></i></option>
                            <option data-data='{"image": "/backend/images/flags/io.svg"}' value="io">io<i class="flag flag-io"></i></option>
                            <option data-data='{"image": "/backend/images/flags/iq.svg"}' value="iq">iq<i class="flag flag-iq"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ir.svg"}' value="ir">ir<i class="flag flag-ir"></i></option>
                            <option data-data='{"image": "/backend/images/flags/is.svg"}' value="is">is<i class="flag flag-is"></i></option>
                            <option data-data='{"image": "/backend/images/flags/it.svg"}' value="it">it<i class="flag flag-it"></i></option>
                            <option data-data='{"image": "/backend/images/flags/je.svg"}' value="je">je<i class="flag flag-je"></i></option>
                            <option data-data='{"image": "/backend/images/flags/jm.svg"}' value="jm">jm<i class="flag flag-jm"></i></option>
                            <option data-data='{"image": "/backend/images/flags/jo.svg"}' value="jo">jo<i class="flag flag-jo"></i></option>
                            <option data-data='{"image": "/backend/images/flags/jp.svg"}' value="jp">jp<i class="flag flag-jp"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ke.svg"}' value="ke">ke<i class="flag flag-ke"></i></option>
                            <option data-data='{"image": "/backend/images/flags/kg.svg"}' value="kg">kg<i class="flag flag-kg"></i></option>
                            <option data-data='{"image": "/backend/images/flags/kh.svg"}' value="kh">kh<i class="flag flag-kh"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ki.svg"}' value="ki">ki<i class="flag flag-ki"></i></option>
                            <option data-data='{"image": "/backend/images/flags/km.svg"}' value="km">km<i class="flag flag-km"></i></option>
                            <option data-data='{"image": "/backend/images/flags/kn.svg"}' value="kn">kn<i class="flag flag-kn"></i></option>
                            <option data-data='{"image": "/backend/images/flags/kp.svg"}' value="kp">kp<i class="flag flag-kp"></i></option>
                            <option data-data='{"image": "/backend/images/flags/kr.svg"}' value="kr">kr<i class="flag flag-kr"></i></option>
                            <option data-data='{"image": "/backend/images/flags/kw.svg"}' value="kw">kw<i class="flag flag-kw"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ky.svg"}' value="ky">ky<i class="flag flag-ky"></i></option>
                            <option data-data='{"image": "/backend/images/flags/kz.svg"}' value="kz">kz<i class="flag flag-kz"></i></option>
                            <option data-data='{"image": "/backend/images/flags/la.svg"}' value="la">la<i class="flag flag-la"></i></option>
                            <option data-data='{"image": "/backend/images/flags/lb.svg"}' value="lb">lb<i class="flag flag-lb"></i></option>
                            <option data-data='{"image": "/backend/images/flags/lc.svg"}' value="lc">lc<i class="flag flag-lc"></i></option>
                            <option data-data='{"image": "/backend/images/flags/li.svg"}' value="li">li<i class="flag flag-li"></i></option>
                            <option data-data='{"image": "/backend/images/flags/lk.svg"}' value="lk">lk<i class="flag flag-lk"></i></option>
                            <option data-data='{"image": "/backend/images/flags/lr.svg"}' value="lr">lr<i class="flag flag-lr"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ls.svg"}' value="ls">ls<i class="flag flag-ls"></i></option>
                            <option data-data='{"image": "/backend/images/flags/lt.svg"}' value="lt">lt<i class="flag flag-lt"></i></option>
                            <option data-data='{"image": "/backend/images/flags/lu.svg"}' value="lu">lu<i class="flag flag-lu"></i></option>
                            <option data-data='{"image": "/backend/images/flags/lv.svg"}' value="lv">lv<i class="flag flag-lv"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ly.svg"}' value="ly">ly<i class="flag flag-ly"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ma.svg"}' value="ma">ma<i class="flag flag-ma"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mc.svg"}' value="mc">mc<i class="flag flag-mc"></i></option>
                            <option data-data='{"image": "/backend/images/flags/md.svg"}' value="md">md<i class="flag flag-md"></i></option>
                            <option data-data='{"image": "/backend/images/flags/me.svg"}' value="me">me<i class="flag flag-me"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mf.svg"}' value="mf">mf<i class="flag flag-mf"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mg.svg"}' value="mg">mg<i class="flag flag-mg"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mh.svg"}' value="mh">mh<i class="flag flag-mh"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mk.svg"}' value="mk">mk<i class="flag flag-mk"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ml.svg"}' value="ml">ml<i class="flag flag-ml"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mm.svg"}' value="mm">mm<i class="flag flag-mm"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mn.svg"}' value="mn">mn<i class="flag flag-mn"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mo.svg"}' value="mo">mo<i class="flag flag-mo"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mp.svg"}' value="mp">mp<i class="flag flag-mp"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mq.svg"}' value="mq">mq<i class="flag flag-mq"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mr.svg"}' value="mr">mr<i class="flag flag-mr"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ms.svg"}' value="ms">ms<i class="flag flag-ms"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mt.svg"}' value="mt">mt<i class="flag flag-mt"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mu.svg"}' value="mu">mu<i class="flag flag-mu"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mv.svg"}' value="mv">mv<i class="flag flag-mv"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mw.svg"}' value="mw">mw<i class="flag flag-mw"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mx.svg"}' value="mx">mx<i class="flag flag-mx"></i></option>
                            <option data-data='{"image": "/backend/images/flags/my.svg"}' value="my">my<i class="flag flag-my"></i></option>
                            <option data-data='{"image": "/backend/images/flags/mz.svg"}' value="mz">mz<i class="flag flag-mz"></i></option>
                            <option data-data='{"image": "/backend/images/flags/na.svg"}' value="na">na<i class="flag flag-na"></i></option>
                            <option data-data='{"image": "/backend/images/flags/nc.svg"}' value="nc">nc<i class="flag flag-nc"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ne.svg"}' value="ne">ne<i class="flag flag-ne"></i></option>
                            <option data-data='{"image": "/backend/images/flags/nf.svg"}' value="nf">nf<i class="flag flag-nf"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ng.svg"}' value="ng">ng<i class="flag flag-ng"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ni.svg"}' value="ni">ni<i class="flag flag-ni"></i></option>
                            <option data-data='{"image": "/backend/images/flags/nl.svg"}' value="nl">nl<i class="flag flag-nl"></i></option>
                            <option data-data='{"image": "/backend/images/flags/no.svg"}' value="no">no<i class="flag flag-no"></i></option>
                            <option data-data='{"image": "/backend/images/flags/np.svg"}' value="np">np<i class="flag flag-np"></i></option>
                            <option data-data='{"image": "/backend/images/flags/nr.svg"}' value="nr">nr<i class="flag flag-nr"></i></option>
                            <option data-data='{"image": "/backend/images/flags/nu.svg"}' value="nu">nu<i class="flag flag-nu"></i></option>
                            <option data-data='{"image": "/backend/images/flags/nz.svg"}' value="nz">nz<i class="flag flag-nz"></i></option>
                            <option data-data='{"image": "/backend/images/flags/om.svg"}' value="om">om<i class="flag flag-om"></i></option>
                            <option data-data='{"image": "/backend/images/flags/pa.svg"}' value="pa">pa<i class="flag flag-pa"></i></option>
                            <option data-data='{"image": "/backend/images/flags/pe.svg"}' value="pe">pe<i class="flag flag-pe"></i></option>
                            <option data-data='{"image": "/backend/images/flags/pf.svg"}' value="pf">pf<i class="flag flag-pf"></i></option>
                            <option data-data='{"image": "/backend/images/flags/pg.svg"}' value="pg">pg<i class="flag flag-pg"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ph.svg"}' value="ph">ph<i class="flag flag-ph"></i></option>
                            <option data-data='{"image": "/backend/images/flags/pk.svg"}' value="pk">pk<i class="flag flag-pk"></i></option>
                            <option data-data='{"image": "/backend/images/flags/pl.svg"}' value="pl">pl<i class="flag flag-pl"></i></option>
                            <option data-data='{"image": "/backend/images/flags/pm.svg"}' value="pm">pm<i class="flag flag-pm"></i></option>
                            <option data-data='{"image": "/backend/images/flags/pn.svg"}' value="pn">pn<i class="flag flag-pn"></i></option>
                            <option data-data='{"image": "/backend/images/flags/pr.svg"}' value="pr">pr<i class="flag flag-pr"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ps.svg"}' value="ps">ps<i class="flag flag-ps"></i></option>
                            <option data-data='{"image": "/backend/images/flags/pt.svg"}' value="pt">pt<i class="flag flag-pt"></i></option>
                            <option data-data='{"image": "/backend/images/flags/pw.svg"}' value="pw">pw<i class="flag flag-pw"></i></option>
                            <option data-data='{"image": "/backend/images/flags/py.svg"}' value="py">py<i class="flag flag-py"></i></option>
                            <option data-data='{"image": "/backend/images/flags/qa.svg"}' value="qa">qa<i class="flag flag-qa"></i></option>
                            <option data-data='{"image": "/backend/images/flags/re.svg"}' value="re">re<i class="flag flag-re"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ro.svg"}' value="ro">ro<i class="flag flag-ro"></i></option>
                            <option data-data='{"image": "/backend/images/flags/rs.svg"}' value="rs">rs<i class="flag flag-rs"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ru.svg"}' value="ru">ru<i class="flag flag-ru"></i></option>
                            <option data-data='{"image": "/backend/images/flags/rw.svg"}' value="rw">rw<i class="flag flag-rw"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sa.svg"}' value="sa">sa<i class="flag flag-sa"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sb.svg"}' value="sb">sb<i class="flag flag-sb"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sc.svg"}' value="sc">sc<i class="flag flag-sc"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sd.svg"}' value="sd">sd<i class="flag flag-sd"></i></option>
                            <option data-data='{"image": "/backend/images/flags/se.svg"}' value="se">se<i class="flag flag-se"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sg.svg"}' value="sg">sg<i class="flag flag-sg"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sh.svg"}' value="sh">sh<i class="flag flag-sh"></i></option>
                            <option data-data='{"image": "/backend/images/flags/si.svg"}' value="si">si<i class="flag flag-si"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sj.svg"}' value="sj">sj<i class="flag flag-sj"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sk.svg"}' value="sk">sk<i class="flag flag-sk"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sl.svg"}' value="sl">sl<i class="flag flag-sl"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sm.svg"}' value="sm">sm<i class="flag flag-sm"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sn.svg"}' value="sn">sn<i class="flag flag-sn"></i></option>
                            <option data-data='{"image": "/backend/images/flags/so.svg"}' value="so">so<i class="flag flag-so"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sr.svg"}' value="sr">sr<i class="flag flag-sr"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ss.svg"}' value="ss">ss<i class="flag flag-ss"></i></option>
                            <option data-data='{"image": "/backend/images/flags/st.svg"}' value="st">st<i class="flag flag-st"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sv.svg"}' value="sv">sv<i class="flag flag-sv"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sx.svg"}' value="sx">sx<i class="flag flag-sx"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sy.svg"}' value="sy">sy<i class="flag flag-sy"></i></option>
                            <option data-data='{"image": "/backend/images/flags/sz.svg"}' value="sz">sz<i class="flag flag-sz"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tc.svg"}' value="tc">tc<i class="flag flag-tc"></i></option>
                            <option data-data='{"image": "/backend/images/flags/td.svg"}' value="td">td<i class="flag flag-td"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tf.svg"}' value="tf">tf<i class="flag flag-tf"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tg.svg"}' value="tg">tg<i class="flag flag-tg"></i></option>
                            <option data-data='{"image": "/backend/images/flags/th.svg"}' value="th">th<i class="flag flag-th"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tj.svg"}' value="tj">tj<i class="flag flag-tj"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tk.svg"}' value="tk">tk<i class="flag flag-tk"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tl.svg"}' value="tl">tl<i class="flag flag-tl"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tm.svg"}' value="tm">tm<i class="flag flag-tm"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tn.svg"}' value="tn">tn<i class="flag flag-tn"></i></option>
                            <option data-data='{"image": "/backend/images/flags/to.svg"}' value="to">to<i class="flag flag-to"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tr.svg"}' value="tr">tr<i class="flag flag-tr"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tt.svg"}' value="tt">tt<i class="flag flag-tt"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tv.svg"}' value="tv">tv<i class="flag flag-tv"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tw.svg"}' value="tw">tw<i class="flag flag-tw"></i></option>
                            <option data-data='{"image": "/backend/images/flags/tz.svg"}' value="tz">tz<i class="flag flag-tz"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ua.svg"}' value="ua">ua<i class="flag flag-ua"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ug.svg"}' value="ug">ug<i class="flag flag-ug"></i></option>
                            <option data-data='{"image": "/backend/images/flags/um.svg"}' value="um">um<i class="flag flag-um"></i></option>
                            <option data-data='{"image": "/backend/images/flags/un.svg"}' value="un">un<i class="flag flag-un"></i></option>
                            <option data-data='{"image": "/backend/images/flags/us.svg"}' value="us">us<i class="flag flag-us"></i></option>
                            <option data-data='{"image": "/backend/images/flags/uy.svg"}' value="uy">uy<i class="flag flag-uy"></i></option>
                            <option data-data='{"image": "/backend/images/flags/uz.svg"}' value="uz">uz<i class="flag flag-uz"></i></option>
                            <option data-data='{"image": "/backend/images/flags/va.svg"}' value="va">va<i class="flag flag-va"></i></option>
                            <option data-data='{"image": "/backend/images/flags/vc.svg"}' value="vc">vc<i class="flag flag-vc"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ve.svg"}' value="ve">ve<i class="flag flag-ve"></i></option>
                            <option data-data='{"image": "/backend/images/flags/vg.svg"}' value="vg">vg<i class="flag flag-vg"></i></option>
                            <option data-data='{"image": "/backend/images/flags/vi.svg"}' value="vi">vi<i class="flag flag-vi"></i></option>
                            <option data-data='{"image": "/backend/images/flags/vn.svg"}' value="vn">vn<i class="flag flag-vn"></i></option>
                            <option data-data='{"image": "/backend/images/flags/vu.svg"}' value="vu">vu<i class="flag flag-vu"></i></option>
                            <option data-data='{"image": "/backend/images/flags/wf.svg"}' value="wf">wf<i class="flag flag-wf"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ws.svg"}' value="ws">ws<i class="flag flag-ws"></i></option>
                            <option data-data='{"image": "/backend/images/flags/ye.svg"}' value="ye">ye<i class="flag flag-ye"></i></option>
                            <option data-data='{"image": "/backend/images/flags/yt.svg"}' value="yt">yt<i class="flag flag-yt"></i></option>
                            <option data-data='{"image": "/backend/images/flags/za.svg"}' value="za">za<i class="flag flag-za"></i></option>
                            <option data-data='{"image": "/backend/images/flags/zm.svg"}' value="zm">zm<i class="flag flag-zm"></i></option>
                            <option data-data='{"image": "/backend/images/flags/zw.svg"}' value="zw">zw<i class="flag flag-zw"></i></option>
                          </select>
                        </div>
                        <button class="continue-form mt-5 mb-5">Сохранить</button>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
<script>
      $(function() {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
      });
</script>
@endsection
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
@endsection
