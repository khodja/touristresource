@extends('layouts.backend')
@section('content')
@include('partials.admin_header')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить Публикацию</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('PublicationController@update',$data->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                        <div class="form-group">
                            <label class="form-label">Название (RU)</label>
                            <input type="text" required="required" class="form-control" name="name_ru" value="{{ $data->name_ru  }}">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Название (EN)</label>
                            <input type="text" required="required" class="form-control" name="name_en" value="{{ $data->name_en  }}">
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label class="form-label">Изображение Публикации</label>--}}
{{--                            <input type="file" required="required" class="form-control" name="image">--}}
{{--                        </div>--}}
                        <div class="form-group">
                            <label class="form-label" for="text1">Текст 1 (RU)</label>
                            <textarea name="text1_ru" class="form-control" id="text1" cols="30" rows="10">
                                {{ $data->text1_ru }}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="text2">Текст 2 (RU)</label>
                            <textarea name="text2_ru" class="form-control" id="text2" cols="30" rows="10">
                                {{ $data->text2_ru }}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="text3">Текст 3 (RU)</label>
                            <textarea name="text3_ru" class="form-control" id="text3" cols="30" rows="10">
                                {{ $data->text3_ru }}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="text1">Текст 1 (EN)</label>
                            <textarea name="text1_en" class="form-control" id="text1" cols="30" rows="10">
                                {{ $data->text1_en }}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="text2">Текст 2 (EN)</label>
                            <textarea name="text2_en" class="form-control" id="text2" cols="30" rows="10">
                                {{ $data->text2_en }}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="text3">Текст 3 (EN)</label>
                            <textarea name="text3_en" class="form-control" id="text3" cols="30" rows="10">
                                {{ $data->text3_en }}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Выберите город </label>
                            <select name="city_id" class="form-control">
                                @foreach( $city as $datas )
                                <option value="{{ $datas->id }}" @if($datas->id == $data->city_id) selected @endif>{{ $datas->name_ru }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Выберите категорию</label>
                            <select name="category_id" class="form-control">
                                @foreach( $category as $datas )
                                <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdn.tiny.cloud/1/qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc/tinymce/5/tinymce.min.js"></script>
<script>
tinymce.init({
  selector: 'textarea',
  height: 500,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
  ],
  toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
});
</script>
@endsection
