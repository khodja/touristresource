@extends('layouts.backend')

@section('content')
@include(Auth::user()->hasRole('admin') ? 'partials.admin_header':'partials.header')
<div class="my-3 my-md-5">
    <div class="container">
        <div class="card">
            <div class="card-header">Добавить Сервис</div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{ action('RoomServiceController@store') }}" class="reg-form" method="POST" enctype="multipart/form-data">
                @csrf
                    <div>
                        <div class="form-group">
                            <label class="form-label" for="name">Название(RU)</label>
                            <input required="required" name="name_ru" type="text" class="form-control" id="name" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="name-en">Название(EN)</label>
                            <input required="required" name="name_en" type="text" class="form-control" id="name-en" placeholder="Введите название">
                        </div>
                    </div>
                    <button class="continue-form mt-5 mb-5">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
