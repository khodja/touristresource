@extends('layouts.backend')

@section('content')
@include('partials.header')
<section>
    <div class="container">
        <div class="row">
        <form action="{{ action('RoomController@update', $data->id) }}" method="POST" enctype="multipart/form-data" id="form">
            @method('PUT')
            @csrf
                <div class="white-block mb-30">
                    <div class="head">
                        <h3>Номер</h3>
                    </div>
                    <div class="content secondary">
                        <div class="input-block">
                        <div class="input">
                          <p>Выберите тип номера:</p>
                            <select name="room_type_id" id="select_type_number" class="custom-select" disabled>
                                <option value="{{ $data->id }}">{{ $data->room_type->name_ru }}</option>
                            </select>
                        </div>
                        </div>
                          <div class="input-block">
                            <div class="input">
                            <p>Название номера</p>
                            <select name="room_name_id" id="select_name_number" class="custom-select" disabled>
                                <option value="{{ $data->id }}">{{ $data->room_name->name_ru }}</option>
                            </select>
                            </div>
                            <div class="text-block max-992">
                              <p>
                                Название будет отображаться на странице отеля
                              </p>
                            </div>
                          </div>
                        <div class="input-block">
                            <div class="input half">
                                <div class="half-block max-992 mobile-mb-30">
                                    <p>Кол-во номеров данного типа</p>
                                    <input required="required" disabled type="number" name="room_count" value="{{$data->room_count}}" class="form-control" min="1" max="100" id="internalNumber">
                                </div>
                                <div class="half-block max-992">
                                    <p>Максимальная вместимость чел.</p>
                                    <input required="required" type="number" disabled name="capacity" value="{{$data->capacity}}" class="form-control" min="1" max="20" id="personNumber">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="white-block mb-30" id="personNumberPriceBlock">
                <div class="head">
                    <h3>Укажите стоимость размещения с учетом всех налогов</h3>
                </div>
                <div class="content secondary d-flex">
                    <div class="input-block inner col-lg-8 px-0 d-flex flex-wrap"></div>
                </div>
                </div>
                <div class="white-block mb-30">
                <div class="head">
                    <h3>Внутренняя нумерация</h3>
                </div>
                <div class="content secondary numeration_changer" id="numerationBlock">
                    <p class="content-text mb-30">Если указать внутреннюю нумерацию, вы сможете размещать гостей согласно желаемому порядковму номеру.</p>
                        <ul class="radio-switch" id="numeration_enabled">
                            <li class="item">
                                <input type="radio" id="radio1" name="numeration_enabled" disabled value="0" @if(!$data->numeration_enabled) checked="" @endif >
                                <label for="radio1">Не указывать</label>
                            </li>
                            <li class="item">
                                <input type="radio" id="radio2" name="numeration_enabled" disabled value="1" @if($data->numeration_enabled) checked="" @endif >
                                <label for="radio2">Указать</label>
                            </li>
                        </ul>
                    <div>
                        @if($data->numeration_enabled)
                        <div class="numeration">
                        </div>
                        @endif
                    </div>
                </div>
                </div>
                <div class="white-block mb-30 changable" id="badsBlock">
                <div class="head">
                    <h3>Наличие кроватей</h3>
                </div>
                    <div class="content">
                        @foreach(json_decode($data->bed_count,true) as $key => $count)
                        <div class="input-block with-trash">
                            <div class="input half">
                                <div class="half-block max-992 mobile-mb-30">
                                    <p>Тип кровати</p>
                                    <select  class="form-control custom-select h-auto" name="bed[]">
                                        @foreach($bed as $item)
                                            <option value="{{$item->id}}" @if(in_array($item->id ,json_decode($data->bed,true))) selected @endif>{{ $item->name_ru  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="half-block max-992">
                                    <p class="second">Количество</p>
                                    <input required="required" type="number" value="{{$count}}" name="bed_count[]" class="form-control w-100" min="1" />
                                </div>
                            </div>
                            @if($key != 0)
                            <div class="text-block">
                                <a href="#" class="btnRemoveBad"><i class="fa-lg fe fe-trash-2"></i></a>
                            </div>
                            @endif
                        </div>
                        @endforeach
                        <div class="input-block with-trash text-block">
                            <span class="add-button d-flex align-items-center" style="height: 42px;" id="btnAddBads"><i class="fe fe-plus-circle"></i> <span>Добавить кровать</span></span>
                        </div>
                    </div>
                </div>
                <div class="white-block mb-30 changable">
                <div class="head">
                    <h3>Предоставляемые услуги и удобства</h3>
                </div>
                <div class="accordion-content">
                    <div class="head-accordion">
                        <p>Укажите прилагаемые к номеру услуги и удобства. Отметье, если они оплачиваются отдельно.</p>
                        <div class="bottom-text">
                            <span>Тип услуг</span>
                            <span>Оплачивается отдельно</span>
                        </div>
                    </div>
                </div>

                  <div class="card-body-form">
                      <div class="accordion" id="accordion">
                        @foreach($service as $item)
                          <div class="card">
                            <div class="card-header">
                              <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#acc{{$item->id}}" aria-expanded="false" aria-controls="{{$item->id}}">
                                    <span><i class="fe fe-plus"></i></span> {{ $item->name_ru }}
                                </button>
                              </h2>
                            </div>
                            <div id="acc{{$item->id}}" class="collapse" aria-labelledby="heading{{$item->id}}" data-parent="#accordion">
                              <div class="card-body p-0">
                                @foreach($item->inners as $inner)
                                <div class="itemServices checkboxes">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input action" name="service[]" @if(!empty(json_decode($data->services,true)[$inner->id])) @if(json_decode($data->services,true)[$inner->id] > 0) checked  value="[{{json_decode($data->services,true)[$inner->id]}},{{$inner->id}}]" @endif @else value="[0,{{$inner->id}}]" @endif data-info="{{ $inner->id }}" />
                                        <span class="custom-control-label">{{ $inner->name_ru }}</span>
                                    </label>
                                    <label class="custom-switch">
                                        <input type="checkbox" value="1" class="custom-switch-input changer" @if(!empty(json_decode($data->services,true)[$inner->id])) @if(json_decode($data->services,true)[$inner->id] > 1) checked @endif @endif />
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                                @endforeach
                                </div>
                            </div>
                          </div>
                        @endforeach
                        </div>
                  </div>
                </div>
          <div class="white-block mb-30 changable">
            <div class="head">
                <h3>Фотографии номера</h3>
            </div>
            <div class="card-header-after">
              <p class="pt-3">Добавьте фотографии номера</p>
              <div class="fotoUploader">
                <ul class="jFiler-items-list jFiler-items-grid d-flex align-items-center">
                    @foreach($images as $image)
                    <li class="gallryUploadBlock_item photo-thumbler" data-image="{{basename($image)}}">
                        <div class="jFiler-item-thumb-image">
                            <img src="{{asset($image)}}" alt="image">
                        </div>
                        <div class="removeItem">
                            <span class="deletePhoto" data-image="{{basename($image)}}">
                                <i class="fe fe-minus"></i>
                            </span>
                        </div>
                    </li>
                    @endforeach
                </ul>
                  <input type="file" name="image[]" class="filer_input3" multiple="multiple" />
              </div>
            </div>
          </div>
            <div class="button-block changable pb-5 reg-form">
                <button class="continue-btn" name="confirm" value="end" type="submit">Сохранить</button>
                <a href="{{ url()->current() }}" class="blue-text ml-40">Отменить изменения</a>
            </div>
        </form>
    </div>
    </div>
</section>
@endsection

@section('script')
<script>
let personNumberInput = $('#personNumber');
let personNumberPriceBlock = $('#personNumberPriceBlock');
let internalNumber = $('#internalNumber');
let numerationBlock = $('#numerationBlock');
const pasteElementInput = (index, blockName , value) =>{
    if(blockName =='personNumber'){
      return (`
                <div class="input half w-auto col-lg-6 pl-0 pb-md-5">
                    <div class="half-block input-group max-992 mobile-mb-30 w-auto">
                      <p>Стоимость размещения ${index} чел.</p>
                        <span class="input-group-prepend" id="basic-addon1">
                          <span class="input-group-text">USD</span>
                        </span>
                        <input type="number" name="price[]" value="${value}" required="required" class="form-control w-auto" >
                    </div>
                </div>`
      );
    }else{
      return (`<div class="input-block">
                    <div class="input half">
                      <div class="half-block">
                          <input type="number" name="numeration[]"  value="${index}" disabled  class="disabled" >
                      </div>
                      <div class="half-block">
                          <input type="number" required="required" name="numeration[]"  class="disabled"  value="${value}" >
                      </div>
                    </div>
              </div>`
      );
    }
}
// personNumberPriceBlock.hide()
// numerationBlock.hide()

$(document).ready(()=>{
$(".filer_input3").filer({
  limit: null,
  maxSize: null,
  extensions: null,
  changeInput: '<a class="btnAddImageToGallery galleryAddElement"><i class="fe fe-plus"></i></a>',
  showThumbs: true,
  theme: "dragdropbox",
  templates: {
    box: '<ul class="jFiler-items-list buttonAdder jFiler-items-grid"></ul>',
    item: `<li class="gallryUploadBlock_item jFiler-item">
    {{'{{fi-image}'.'}'}}
    <div class="removeItem"  ><span><i class="fe fe-minus"></i></span></div>
    </li>`,
    progressBar: '<div class="bar"></div>',
    itemAppendToEnd: true,
    canvasImage: true,
    removeConfirmation: false,
    _selectors: {
      list: '.jFiler-items-list',
      item: '.jFiler-item',
      progressBar: '.bar',
      remove: '.fe.fe-minus'
    }
  },
  dragDrop: {
    dragEnter: null,
    dragLeave: null,
    drop: null,
    dragContainer: null,
  },
  files: null,
  addMore: true,
  allowDuplicates: false,
  clipBoardPaste: true,
  excludeName: null,
  beforeRender: null,
  afterRender: null,
  beforeShow: null,
  beforeSelect: null,
  itemAppendToEnd: true,
  onSelect: null,
  afterShow: function(jqEl, htmlEl, parentEl, itemEl){
    $('.galleryAddElement').hide()
    $('.cloneGalleryAddElement').remove()
    $('.buttonAdder').append('<a class="btnAddImageToGallery cloneGalleryAddElement"><i class="fe fe-plus"></i></a>')
  },
  onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
    let filerKit = inputEl.prop("jFiler"),
        file_name = filerKit.files_list[id].name;
      if(filerKit.files_list.length == 1){
        $('.galleryAddElement').show()
      }
  },
  onEmpty: null,
  options: null,
});
$('.uploader').on('click', '.cloneGalleryAddElement', function(e){
  $('.galleryAddElement').trigger('click');
});
$('.deletePhoto').click((e) =>{
    let removeImage = $(e.target);
    let imageData = removeImage.data('image');
    let deleteItem = $.get("{{action('RoomController@removeImage',$id)}}" , {file_name: imageData});
    deleteItem.done(()=>{
        removeImage.parent().parent().remove();
    });
});
      let person = personNumberInput;
      let personVal = person.val();
      let data = JSON.parse(@json($data->price));
      $('#personNumberPriceBlock .inner').empty();
      if(personVal != '' && personVal >= 1){
        for (let index = 0; index < personVal; index++) {
          $('#personNumberPriceBlock .inner').append(pasteElementInput(index+1, 'personNumber',(data[index] !== undefined ? data[index] : ' ')))
        }
        personNumberPriceBlock.show()
      }else{
        personNumberPriceBlock.hide()
      }
  let numeration = internalNumber;
  let numerationVal = numeration.val();
  let numerationData = JSON.parse(@json($data->numeration));
  $('#numerationBlock .numeration').empty();
  if(numerationVal != '' && numerationVal >= 1){
    for (let index = 0; index < numerationVal; index++) {
      let final = $('#numerationBlock .numeration').append(pasteElementInput(index+1, 'internalNumber',(numerationData[index] !== undefined ? numerationData[index] : ' ')));
    }
    numerationBlock.show();
  }else{
    numerationBlock.hide();
  }
});
let btnAddBads = $('#btnAddBads');
let btnRemoveBad = $('.btnRemoveBad');
let elementBadHtml = `<div class="input-block with-trash">
                    <div class="input half">
                        <div class="half-block max-992 mobile-mb-30">
                            <p>Тип кровати</p>
                            <div class="d-inline-block selectize-control">
                            <select class="form-control custom-select h-auto" name="bed[]">
                                @foreach($bed as $item)
                                    <option value="{{ $item->id  }}">{{ $item->name_ru  }}</option>
                                @endforeach
                            </select>
                            </div>
                            <span class="btnRemoveBad trash-icon"><i class="fa-lg fe fe-trash-2"></i></span>
                        </div>
                        <div class="half-block max-992">
                            <p>Количество</p>
                            <input required=required" type="number" name="bed_count[]" min="1" value="1">
                        </div>
                    </div>
                    <div class="text-block">
                        <a href="#" class="btnRemoveBad"><i class="fa-lg fe fe-trash-2"></i></a>
                    </div>
                    </div>`;
btnAddBads.on('click', function() {
  $(this).parent().before(elementBadHtml)
})
$('#badsBlock').on('click', '.btnRemoveBad', function(e) {
  e.preventDefault();
  $(this).closest('.with-trash').remove();
})

$('.btn_saveOrFinish').on('click', function(e) {
  e.preventDefault();
})
let checkboxes = $('.checkboxes');
checkboxes.each(function() {
    let action = $(this).find('.action');
    let changer = ($(this).find('.changer'));
    changer.click(function(){
        let left = $(this).parent().parent().find('.action');
        let right = $(this).parent().parent().find('.changer');
        if(left.prop('checked') && right.prop('checked')){
            left.val(`[2,${left.data('info')}]`);
            left.prop('checked',true);
            right.prop('checked',true);
        }
        if(left.prop('checked') && !right.prop('checked')){
            left.val(`[1,${left.data('info')}]`);
            left.prop('checked',true);
            right.prop('checked',false);
        }
        if(!left.prop('checked') && !right.prop('checked')){
            left.val(`[0,${left.data('info')}]`);
            left.prop('checked',false);
            right.prop('checked',false);
        }
        if(!left.prop('checked') && right.prop('checked')){
            left.val(`[2,${left.data('info')}]`);
            left.prop('checked',true);
            right.prop('checked',true);
        }
    });
    action.click(function(){
        let left = $(this).parent().parent().find('.action');
        let right = $(this).parent().parent().find('.changer');
        if(left.prop('checked') && right.prop('checked')){
            left.val(`[2,${left.data('info')}]`);
            left.prop('checked',true);
            right.prop('checked',true);
        }
        if(left.prop('checked') && !right.prop('checked')){
            left.val(`[1,${left.data('info')}]`);
            left.prop('checked',true);
            right.prop('checked',false);
        }
        if(!left.prop('checked') && !right.prop('checked')){
            left.val(`[0,${left.data('info')}]`);
            left.prop('checked',false);
            right.prop('checked',false);
        }
        if(!left.prop('checked') && right.prop('checked')){
            left.val(`[0,${left.data('info')}]`);
            left.prop('checked',false);
            right.prop('checked',false);
        }
    });
});
</script>
@endsection
