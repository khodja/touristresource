@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить Бронь</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('CalendarController@store', $id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label class="form-label" for="from">Дата заселения</label>
                            <input name="from" required="required" type="date" class="form-control" id="from" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="to">Дата выезда</label>
                            <input name="to" required="required" type="date" class="form-control" id="to" placeholder="Введите описание">
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
