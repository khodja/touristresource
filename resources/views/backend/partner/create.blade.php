@extends(Auth::user()->hotels()->first() ? 'layouts.backend': 'layouts.app')

@section('content')
@if(Auth::user()->hotels()->first())
    @include('partials.header')
@endif
<section>
    <div class="container">
        <div class="row">
        <form action="{{ action('PartnerController@store', $id) }}" method="POST" enctype="multipart/form-data" id="form">
            @csrf
                <div class="white-block mb-30">
                    <div class="head">
                        <h3>
                            <i class="fa-lg fe fe-plus-circle pr-2"></i>
                            Новый партнер
                        </h3>
                    </div>
                    <div class="content secondary">
                        <div class="input-block">
                        <div class="input">
                          <p>Наименование</p>
                            <input type="text" name="name" class="regStepOne" id="regUserName" required="required" />
                        </div>
                            <div class="text-block">
                                <p>В дальнейшем, вы сможете указывать партнера при добавлении брони.</p>
                            </div>
                        </div>
                        <div class="input-block">
                            <div class="input half">
                                <div class="half-block max-992 mobile-mb-30">
                                    <p>Контактный телефон</p>
                                    <input required="required" type="number" name="phone" class="form-control regStepOne" min="7" id="regUserPhone">
                                </div>
                                <div class="half-block max-992">
                                    <p>E-mail</p>
                                    <input required="required" type="email" name="email" class="form-control regStepOne" id="regUserEmail">
                                </div>
                            </div>
                        </div>
                        <div class="input-block">
                        <div class="input">
                          <p>Реквизиты или другая информация</p>
                            <textarea name="description" cols="30" rows="10"></textarea>
                        </div>
                        </div>
                    </div>
                </div>
            <div class="button-block">
                <button class="continue-btn" type="submit">Добавить партнера</button>
            </div>
        </form>
        </div>
    </div>
</section>
@endsection
