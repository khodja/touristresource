@extends('layouts.backend')

@section('content')
@include('partials.header')
    <section>
		<div class="container">
			<div class="row">
                <h2 class="blue-title">{{ \Carbon\Carbon::parse($booking->from)->format('F d, Y')}} — {{ \Carbon\Carbon::parse($booking->to)->format('F d, Y')}}</h2>
                <form action="{{ action('BookingController@update', $booking->id) }}" method="POST" autocomplete="off">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
						<div class="head">
							<h3>
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-key"><path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path></svg>
                                {{ $room->room_type()->first()->name_ru }} {{ $room->room_name()->first()->name_ru }}
							</h3>
						</div>
						<div class="content">
                            <input type="hidden" name="room_id" value="{{ $room->id }}">
							<div class="input-block">
								<div class="input half">
									<div class="half-block max-992">
                                        @if($room->numeration_enabled)
										<p>Внутренний номер</p>
										<select name="numeration" class="form-control">
                                            <option selected="true" disabled="disabled">Выбрать</option>
                                            @foreach(json_decode($room->numeration,true) as $numeration)
                                                <option value="{{$numeration}}" @if($numeration == $booking->numeration ) selected="true" @endif>{{$numeration}}</option>
                                            @endforeach
										</select>
                                        @else
                                            <p>Нумерация не указана</p>
                                            <input type="number" disabled name="numeration" value="0">
                                        @endif
									</div>
									<div class="half-block max-992 mobile-mb-30">
										<label for="name-guest">Имя гостя</label>
										<input type="text" name="name" id="name-guest" required="required" value="{{$booking->name}}">
									</div>
								</div>

							</div>
							<div class="input-block">
								<div class="input half">
									<div class="half-block max-992">
										<p>Кол-во взрослых</p>
										<select name="adult"  class="form-control custom-select ">
                                            @for($i= 1; $i <= ($room->capacity * 1); $i++)
                                              <option value="{{$i}}" @if($i == $booking->adult ) selected="true" @endif>{{$i}}</option>
                                            @endfor
										</select>
									</div>
									<div class="half-block max-992 mobile-mb-30">
										<p>Кол-во детей</p>
										<select name="child"  class="form-control custom-select ">
										  <option value="0" @if(0 == $booking->child ) selected="true" @endif>0</option>
										  <option value="1" @if(1 == $booking->child ) selected="true" @endif>1</option>
										  <option value="2" @if(2 == $booking->child ) selected="true" @endif>2</option>
										  <option value="3" @if(3 == $booking->child ) selected="true" @endif>3</option>
										</select>
									</div>
								</div>

							</div>
							<div class="input-block">
								<div class="input half">
									<div class="half-block max-992">
										<label for="regUserPhone">Телефон</label>
										<div class="phone">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone phone-icon"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>
											<input type="number" name="phone" type="number" min="0" id="regUserPhone" required="required" value="{{$booking->phone}}">
										</div>
									</div>
									<div class="half-block max-992 mobile-mb-30">
										<label for="email">E-mail</label>
										<input type="email" name="email" id="email" required="required" value="{{$booking->email}}">
									</div>
								</div>

							</div>
							<div class="input-block">
								<div class="input">
									<p>Партнер</p>
                                    <select name="partner_id"  class="form-control custom-select ">
                                      <option value="0">без партнера</option>
                                        @foreach($partners as $partner)
                                          <option value="{{$partner->id}}" @if($partner->id == $booking->partner_id) @endif>{{$partner->name}}</option>
                                        @endforeach
                                    </select>
								</div>
								<div class="text-block">
									<p>Укажите, если бронь поступила от партнера</p>
								</div>
							</div>
						</div>
					</div>
					<div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                        <a href="{{ url()->current() }}" class="blue-text ml-40">Отменить изменения</a>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection

@section('script')
<script>
let __origDefine = define;
define = null;
</script>
<script src="{{ asset('backend/js/vendors/selectize.min.js')}}"></script>
<script>

$('.custom-select').selectize({});
</script>
@endsection
