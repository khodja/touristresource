@extends('layouts.backend')

@section('content')
    <style>
.lightpick {
    position: absolute;
    z-index: 99999;
    padding: 4px;
    border-radius: 4px;
    background-color: #FFF;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.25);
    color: #000;
    font-family: manrope-semibold, sans-serif;
    line-height: 1.125em;
}

.lightpick--inlined {
    position: relative;
    width: 100%;
    margin: 0 auto 0px;
    /*box-shadow: 10px 10px 50px rgba(0, 35, 109, 0.1);*/

}

.lightpick,
.lightpick *,
.lightpick::after,
.lightpick::before {
    box-sizing: border-box;
}

.lightpick.is-hidden {
    display: none;
}

.lightpick__months {
    display: grid;
    background-color: #EEE;
    grid-template-columns: auto;
    grid-gap: 1px;
}

.lightpick--2-columns .lightpick__months {
    grid-template-columns: auto auto;
}

.lightpick--3-columns .lightpick__months {
    grid-template-columns: auto auto auto;
}

.lightpick--4-columns .lightpick__months {
    grid-template-columns: auto auto auto auto;
}

.lightpick--5-columns .lightpick__months {
    grid-template-columns: auto auto auto auto auto;
}

.lightpick__month {
    padding: 4px;
    min-width: 288px;
    background-color: #FFF;
}

.lightpick__month-title-bar {
    display: flex;
    margin-bottom: 4px;
    justify-content: space-between;
    align-items: center;
}

.lightpick__month-title {
    margin-top: 4px;
    margin-bottom: 4px;
    margin-left: 4px;
    font-size: 16px;
    font-weight: normal;
    line-height: 24px;
    cursor: default;
    padding: 0 4px;
    border-radius: 4px;
    color: #00236D;
}

.lightpick__month-title > .lightpick__select {
    border: none;
    background-color: transparent;
    outline: none;
    -moz-appearance: none;
    -webkit-appearance: none;
    appearance: none;
}
.lightpick__month-title > .lightpick__select:disabled {
    color: #00236D;
}

.lightpick__month-title > .lightpick__select-months {
    font-weight: bold;
    font-size: 1em;
    color: #00236D;
    text-transform: capitalize;
    margin-right: .5em;
}

.lightpick__toolbar {
    display: flex;
    text-align: right;
    justify-content: flex-end;
}

.lightpick__previous-action,
.lightpick__next-action,
.lightpick__close-action {
    display: flex;
    margin-left: 6px;
    width: 32px;
    height: 32px;
    outline: none;
    border: none;
    background-color: #DDD;
    justify-content: center;
    align-items: center;
}

.lightpick__previous-action,
.lightpick__next-action {
    font-size: 12px;
}

.lightpick__close-action {
    font-size: 18px;
}

.lightpick__previous-action:active,
.lightpick__next-action:active,
.lightpick__close-action:active {
    color: inherit;
}

.lightpick__days-of-the-week {
    padding-top: 15px;
    display: grid;
    grid-template-columns: repeat(7, 1fr);
}

.lightpick__day-of-the-week {
    display: flex;
    font-size: 12px;
    font-weight: 300;
    text-transform: capitalize;
    justify-content: center;
    align-items: center;
}

.lightpick__days {
    padding-top: 15px;
    display: grid;
    grid-template-columns: repeat(7, 1fr);
}

.lightpick__day {
    display: flex;
    height: 60px;
    background-position: center center;
    background-size: contain;
    background-repeat: no-repeat;
    font-size: 13px;
    justify-content: center;
    align-items: center;
    cursor: default;
}
.lightpick__day.is-available{
    color: #00236D;
}

.lightpick__day.is-today {
    background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32'%3E%3Ccircle fill='rgba(220, 50, 47, .5)' cx='16' cy='16' r='16'/%3E%3C/svg%3E");
    background-size: 18.8% auto;
    background-position: center bottom;
    color: #DC322F;
}

.lightpick__day:not(.is-disabled):hover {
    background: #00236D;
    color: white;
    cursor: pointer;
}

.lightpick__day.is-disabled {
    opacity: 0.38;
    pointer-events: none;
}

.lightpick__day.disabled-tooltip {
    pointer-events: auto;
}

.lightpick__day.is-disabled.is-forward-selected {
    opacity: 1;
}
.lightpick__day.is-disabled.is-forward-selected:not(.is-start-date) {
    background-color: #F4438A;
    color: white;
    background-image: none;
}

.lightpick__day.is-previous-month,
.lightpick__day.is-next-month {
    opacity: 0.38;
}

.lightpick__day.lightpick__day.is-in-range:not(.is-disabled) {
    opacity: 1;
}

.lightpick__day.is-in-range {
    border-radius: 0;
    background-color: #F4438A;
    color: white;
    background-image: none;
}


.lightpick__day.is-start-date.is-in-range,
.lightpick__day.is-end-date.is-in-range.is-flipped {
    background-color: #00236D;
    background-image: none;
}

.lightpick__day.is-end-date.is-in-range,
.lightpick__day.is-start-date.is-in-range.is-flipped {
    background-color: #00236D;
    background-image: none;
}


.lightpick__day.is-start-date,
.lightpick__day.is-end-date,
.lightpick__day.is-start-date:hover,
.lightpick__day.is-end-date:hover {
    background-size: auto;
    background-position: center;
    color: #FFF;
    font-weight: bold;
}

.lightpick__tooltip {
    position: absolute;
    margin-top: -4px;
    padding: 4px 8px;
    border-radius: 4px;
    background-color: #FFF;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.25);
    white-space: nowrap;
    font-size: 11px;
    pointer-events: none;
}

.lightpick__tooltip::before {
    position: absolute;
    bottom: -5px;
    left: calc(50% - 5px);
    border-top: 5px solid rgba(0, 0, 0, 0.12);
    border-right: 5px solid transparent;
    border-left: 5px solid transparent;
    content: "";
}

.lightpick__tooltip::after {
    position: absolute;
    bottom: -4px;
    left: calc(50% - 4px);
    border-top: 4px solid #FFF;
    border-right: 4px solid transparent;
    border-left: 4px solid transparent;
    content: "";
}
.lightpick__footer {
    display: flex;
    justify-content: space-between;
}
.lightpick__reset-action,
.lightpick__apply-action {
    border-radius: 5px;
    font-size: 12px;
    border: none;
}
.lightpick__reset-action {
    color: #fff;
    background-color: #aeacad;
}
.lightpick__apply-action {
    color: #fff;
    background-color: #2495f3;
}
    </style>
@include('partials.header')
<section>
<div class="container">
    <div class="card">
        <div class="card-header justify-content-between">
            <h3 class="card-title">Новая бронь</h3>
                 <a class="colored-btn d-none request" href="{{ action('BookingController@get', $id) }}">Добавить</a>
        </div>
        <div id='calendar' class="card-body pb-7">
            <input name="date" required="required" type="hidden" class="form-control col-sm-4 mr-3" id="date" placeholder="Введите название">
        </div>
    </div>
</div>
</section>
@endsection
@section('style')
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
@endsection
@section('script')
<script>
let __origDefine = define;
define = null;
</script>
<script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('backend/plugins/fullcalendar/js/locales.min.js') }}"></script>
<script src="{{ asset('backend/plugins/fullcalendar/js/moment.min.js') }}"></script>
<script src="{{ asset('backend/js/vendors/lightpick.js') }}"></script>
<script>
let picker = new Lightpick({
    field: document.getElementById('date'),
    singleDate: false,
    minDate: moment(),
    lang: 'ru',
    numberOfMonths: 2,
    inline: true,
    onSelect: function(start, end){
        var params = {
            from: moment(start).format('D-M-Y'),
            to: moment(end).format('D-M-Y')
        };
        let data = [];
        var esc = encodeURIComponent;
        var query = Object.keys(params)
        .map(k => esc(k) + '=' + esc(params[k]))
        .join('&');
        var url = "{{ action('BookingController@get',$id) }}?" + query;
        let str = '';
        str += start ? moment(start).lang("ru").format('D/MM/YYYY') + ' до ' : '';
        str += end ? moment(end).lang("ru").format('D/MM/YYYY') : '...';
        if(end != null && start != null){
            let request = $('.request');
            request.attr('href',url);
            request.removeClass('d-none');
        }
        else{
            request.addClass('d-none');
        }
    }
});
</script>

@endsection
