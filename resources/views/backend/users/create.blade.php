@extends('layouts.backend')

@section('content')
@include('partials.admin_header')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12 px-0">
            <div class="card">
                <div class="card-header">Добавить Ползователя</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('ManagerController@store') }}" method="POST">
                    @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Имя</label>
                            <input name="name" type="text" class="form-control" id="food-name-uz" placeholder="Введите Имя" checked="checked" />
                        </div>
                        <div class="form-group">
                            <label for="food-desc-uz">Email</label>
                            <input name="email" type="email" class="form-control" id="food-desc-uz" placeholder="Введите Email" checked="checked" />
                        </div>
                        <div class="form-group">
                            <label for="phone">Номер</label>
                            <input name="phone" type="text" class="form-control" id="phone" placeholder="Введите Номер" checked="checked" />
                        </div>
                        <div class="form-group">
                            Выберите Роль
                            <select name="role" class="form-control">
                                @foreach( $data as $datas )
                                <option value="{{ $datas->type }}">
                                    @if( $datas->type == 'admin' ) Модератор @endif
                                    @if($datas->type == 'manager') Менеджер @endif
                                    @if($datas->type == 'member') Гость @endif
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="food-desc-ru">Пароль</label>
                            <input name="password" type="text" class="form-control" id="food-desc-ru" placeholder="Введите Пароль" checked="checked" />
                        </div>
                        <button class="btn btn-success">Добавить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
