@extends('layouts.backend')

@section('content')
@include('partials.header')
	<section>
		<div class="container">
			<div class="row">
                <form action="{{ action('PriceController@update', $id) }}" method="POST" enctype="multipart/form-data" id="form">
                @csrf
                @method('PUT')
                <div class="white-block mb-30">
                    <div class="head">
                        <h3>
                            <i class="fa-lg fe fe-dollar-sign pr-2"></i>
                            Изменить все цены
                        </h3>
                    </div>
                    <div class="content">
                        <div class="input-block select-number">
                            <div class="input selectInput">
                                <select id="select-beast1" class="form-control custom-select changer_type">
                                  <option value="1">Увеличить %</option>
                                  <option value="0">Уменьшить %</option>
                                </select>
                                <input type="number" min="0" value="0" max="90" class="changer_number" />
                            </div>
                            <div class="text-block">
                                <p>Вы можете изменить все цены сразу в процентном выражении или указав фикс. сумму</p>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="hotel_id" value="{{$data[0]->hotel->id}}" />
                @foreach($data as $room)

                    <div class="white-block mb-30">
                    <div class="head">
                        <h3>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-key"><path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path></svg>
                            {{ $room->room_type()->first()->name_ru }} {{ $room->room_name()->first()->name_ru }}
                        </h3>
                    </div>
                    <div class="content secondary">
                      <div class="input-block d-flex justify-content-start">
                        @foreach(json_decode($room->price,true) as $key => $price)
                        <div class="input half w-auto col-lg-4 pl-0 pb-md-5">
                            <div class="half-block input-group max-992 mobile-mb-30 w-auto">
                              <p>Стоимость размещения {{$key+1}} чел.</p>
                                <span class="input-group-prepend" id="basic-addon1">
                                  <span class="input-group-text">USD</span>
                                </span>
                                <input type="number" name="price[]" required="required" value="{{$price}}" data-price="{{$price}}" min="0" class="form-control w-auto price-change" maxlength="22">
                            </div>
                        </div>
                        @endforeach
                      </div>
                    </div>
                    </div>
                    @endforeach
                <div class="button-block">
                    <button type="submit" class="continue-btn">Сохранить</button>
                    <a href="{{ url()->current() }}" class="blue-text ml-40">Отменить изменения</a>
                </div>
                </form>
            </div>
		</div>
	</section>
@endsection

@section('script')
<script>
    $('.changer_type').change(()=>changePrice());
    $('.changer_number').change(()=>changePrice());
    let changeablePrices = $('.price-change');
    changePrice = () => {
        let changerType = $('.changer_type').find(':selected').val();
        let changerNumber = $('.changer_number').val();
        let percentage = changerNumber / 100;
        changeablePrices.each(function(){
            let price = $(this).data('price');
            let lastPrice = 0;
            if(changerType == 1){
                lastPrice = parseInt(price) * (1 + percentage);
            }
            else{
                lastPrice = parseInt(price) - (parseInt(price) * (percentage));
            }
            $(this).val(Math.round(lastPrice));
        });

        return 0 ;
    }
</script>
@endsection
