<?php

use Illuminate\Database\Seeder;
use App\RoomName;

class RoomNameTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RoomName::create([
            'name_ru'=>'одноместный улучшенный',
            'name_en'=>'одноместный улучшенный',
            'type_id'=> 1,
        ]);
    }
}
