<?php

use Illuminate\Database\Seeder;
use App\RoomType;
class RoomTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RoomType::create([
            'name_ru'=>'Стандартный',
            'name_en'=>'Стандартный',
            'type_id'=> 1,
        ]);
    }
}
