<?php

use Illuminate\Database\Seeder;
use App\ServiceInner;
use App\RoomServiceInner;
class ServiceInnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServiceInner::create([
            'name_ru'=>'info',
            'name_en'=>'info',
            'service_id'=> 1,
        ]);
        RoomServiceInner::create([
            'name_ru'=>'info',
            'name_en'=>'info',
            'room_service_id'=> 1,
        ]);
    }
}
