<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',255)->nullable();
            $table->string('description',255)->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('type_id')->nullable();
            $table->boolean('transfer')->default(0);
            $table->string('address');
            $table->string('phone');
            $table->unsignedInteger('rating')->nullable();
            $table->unsignedInteger('sale')->default(0);
            $table->boolean('confirmation')->default(0);
            $table->boolean('credit')->default(0);
            $table->time('income');
            $table->time('income_to');
            $table->time('outcome');
            $table->time('outcome_to');
            $table->string('cards');
            $table->boolean('animals')->default(0);
            $table->string('services');
            $table->boolean('status')->default(0);
            $table->boolean('disabled')->default(0);
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
