<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('hotel_id');
            $table->unsignedInteger('room_type_id');
            $table->unsignedInteger('room_name_id');
            $table->string('room_count');
            $table->unsignedInteger('capacity');
            $table->text('numeration');
            $table->boolean('numeration_enabled')->default(0);
            $table->text('bed');
            $table->string('bed_count');
            $table->string('services');
            $table->string('price');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
