<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_ru',255);
            $table->string('name_en',255);
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('category_id');
            $table->text('text1_ru');
            $table->text('text1_en');
            $table->text('text2_ru')->nullable();
            $table->text('text2_en')->nullable();
            $table->text('text3_ru')->nullable();
            $table->text('text3_en')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publications');
    }
}
