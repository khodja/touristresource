<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', 'PageController@index');
Route::get('/hotel/{id}', 'PageController@show');
Route::get('/search', 'PageController@search');
Route::get('/reservation', 'PageController@reservation');
Route::get('/empty', 'PageController@empty');
Route::get('/bookings', 'PageController@bookings');
Route::get('/details/{id}', 'PageController@details');
Route::get('/change/{id}', 'PageController@change');
Route::get('/booking/{id}/confirmation', 'PageController@newDates');
Route::get('/booking/{id}/cancel', 'PageController@cancel');
Route::get('/booking/hotel/{id}/cancel', 'PageController@cancelHotel');
Route::get('/settings', 'PageController@settings');
Route::get('/confirmation/{id}', 'PageController@confirmation');
Route::post('/book', 'PageController@book');
Route::post('/change/book/{id}', 'PageController@changeBook');
Route::post('/booking/cancel', 'PageController@setCancel');

Route::group(['middleware' => ['role:admin,manager'] ], function () {
    Route::get('/dashboard', 'HomeController@index')->name('home');
    Route::get('/dashboard/settings', 'UserController@setting');
    Route::put('/dashboard/settings', 'UserController@update');

    Route::get('/dashboard/news', 'NewsController@index');
    Route::get('/dashboard/news/create', 'NewsController@create');
    Route::get('/dashboard/news/edit/{id}', 'NewsController@edit');
    Route::post('/dashboard/news/store', 'NewsController@store');
    Route::put('/dashboard/news/update/{id}', 'NewsController@update');
    Route::delete('/dashboard/news/delete/{id}', 'NewsController@delete');

    Route::get('/dashboard/manager', 'ManagerController@index');
    Route::get('/dashboard/manager/create', 'ManagerController@create');
    Route::get('/dashboard/manager/edit/{id}', 'ManagerController@edit');
    Route::post('/dashboard/manager/store', 'ManagerController@store');
    Route::put('/dashboard/manager/update/{id}', 'ManagerController@update');
    Route::delete('/dashboard/manager/delete/{id}', 'ManagerController@delete');

    Route::get('/dashboard/categories', 'CategoryController@index');
    Route::get('/dashboard/categories/create', 'CategoryController@create');
    Route::get('/dashboard/categories/edit/{id}', 'CategoryController@edit');
    Route::post('/dashboard/categories/store', 'CategoryController@store');
    Route::put('/dashboard/categories/update/{id}', 'CategoryController@update');
    Route::delete('/dashboard/categories/delete/{id}', 'CategoryController@delete');

    Route::get('/dashboard/bed', 'BedController@index');
    Route::get('/dashboard/bed/create', 'BedController@create');
    Route::get('/dashboard/bed/edit/{id}', 'BedController@edit');
    Route::post('/dashboard/bed/store', 'BedController@store');
    Route::put('/dashboard/bed/update/{id}', 'BedController@update');
    Route::delete('/dashboard/bed/delete/{id}', 'BedController@delete');

    Route::get('/dashboard/service', 'ServiceController@index');
    Route::get('/dashboard/service/create', 'ServiceController@create');
    Route::get('/dashboard/service/edit/{id}', 'ServiceController@edit');
    Route::post('/dashboard/service/store', 'ServiceController@store');
    Route::put('/dashboard/service/update/{id}', 'ServiceController@update');
    Route::delete('/dashboard/service/delete/{id}', 'ServiceController@delete');

    Route::get('/dashboard/service/{id}/inner', 'ServiceInnerController@index');
    Route::get('/dashboard/service/inner/edit/{id}', 'ServiceInnerController@edit');
    Route::get('/dashboard/service/inner/{id}/create', 'ServiceInnerController@create');
    Route::post('/dashboard/service/{id}/inner/store', 'ServiceInnerController@store');
    Route::put('/dashboard/inner/{id}/update', 'ServiceInnerController@update');
    Route::delete('/dashboard/service/inner/{id}', 'ServiceInnerController@delete');

    Route::get('/dashboard/room/service', 'RoomServiceController@index');
    Route::get('/dashboard/room/service/create', 'RoomServiceController@create');
    Route::get('/dashboard/room/service/edit/{id}', 'RoomServiceController@edit');
    Route::post('/dashboard/room/service/store', 'RoomServiceController@store');
    Route::put('/dashboard/room/service/update/{id}', 'RoomServiceController@update');
    Route::delete('/dashboard/room/service/delete/{id}', 'RoomServiceController@delete');

    Route::get('/dashboard/room/service/{id}/inner', 'RoomServiceInnerController@index');
    Route::get('/dashboard/room/service/inner/edit/{id}', 'RoomServiceInnerController@edit');
    Route::get('/dashboard/room/service/inner/{id}/create', 'RoomServiceInnerController@create');
    Route::post('/dashboard/room/service/{id}/inner/store', 'RoomServiceInnerController@store');
    Route::put('/dashboard/room/inner/{id}/update', 'RoomServiceInnerController@update');
    Route::delete('/dashboard/room/service/inner/{id}', 'RoomServiceInnerController@delete');

    Route::get('/dashboard/type', 'TypeController@index');
    Route::get('/dashboard/type/create', 'TypeController@create');
    Route::get('/dashboard/type/edit/{id}', 'TypeController@edit');
    Route::post('/dashboard/type/store', 'TypeController@store');
    Route::put('/dashboard/type/update/{id}', 'TypeController@update');
    Route::delete('/dashboard/type/delete/{id}', 'TypeController@delete');

    Route::get('/dashboard/type/{id}/room-type', 'RoomTypeController@index');
    Route::get('/dashboard/type/room-type/edit/{id}', 'RoomTypeController@edit');
    Route::get('/dashboard/type/room-type/{id}/create', 'RoomTypeController@create');
    Route::post('/dashboard/type/{id}/room-type/store', 'RoomTypeController@store');
    Route::put('/dashboard/room-type/{id}/update', 'RoomTypeController@update');
    Route::delete('/dashboard/type/room-type/{id}', 'RoomTypeController@delete');

    Route::get('/dashboard/room-type/{id}/name', 'RoomNameController@index');
    Route::get('/dashboard/room-type/name/edit/{id}', 'RoomNameController@edit');
    Route::get('/dashboard/room-type/name/{id}/create', 'RoomNameController@create');
    Route::post('/dashboard/room-type/{id}/name/store', 'RoomNameController@store');
    Route::put('/name/{id}/update', 'RoomNameController@update');
    Route::delete('/dashboard/room-type/name/{id}', 'RoomNameController@delete');

    Route::get('/dashboard/publication', 'PublicationController@index');
    Route::get('/dashboard/publication/create', 'PublicationController@create');
    Route::get('/dashboard/publication/edit/{id}', 'PublicationController@edit');
    Route::post('/dashboard/publication/store', 'PublicationController@store');
    Route::put('/dashboard/publication/update/{id}', 'PublicationController@update');
    Route::delete('/dashboard/publication/delete/{id}', 'PublicationController@delete');


    Route::get('/dashboard/country/{id}/city', 'CityController@index');
    Route::get('/dashboard/country/{id}/city/create', 'CityController@create');
    Route::get('/dashboard/country/{id}/city/edit', 'CityController@edit');
    Route::post('/dashboard/country/{id}/city/store', 'CityController@store');
    Route::put('/dashboard/country/{id}/city/update', 'CityController@update');
    Route::delete('/dashboard/country/{id}/city/delete', 'CityController@delete');

    Route::get('/dashboard/country', 'CountryController@index');
    Route::get('/dashboard/country/create', 'CountryController@create');
    Route::get('/dashboard/country/edit/{id}', 'CountryController@edit');
    Route::post('/dashboard/country/store', 'CountryController@store');
    Route::put('/dashboard/country/update/{id}', 'CountryController@update');
    Route::delete('/dashboard/country/delete/{id}', 'CountryController@delete');

    Route::get('/dashboard/hotel', 'HotelController@index');
    Route::get('/dashboard/hotel/create', 'HotelController@create');
    Route::get('/dashboard/hotel/edit/{id}', 'HotelController@edit');
    Route::get('/dashboard/hotel/switch/{id}', 'HotelController@switch');
    Route::get('/dashboard/hotel/removeImage/{id}', 'HotelController@removeImage');
    Route::put('/dashboard/hotel/update/{id}', 'HotelController@update');
    Route::post('/dashboard/hotel/store', 'HotelController@store');

    Route::get('/dashboard/hotel/{id}/rooms', 'RoomController@index');
    Route::get('/dashboard/hotel/rooms/edit/{id}', 'RoomController@edit');
    Route::get('/dashboard/hotel/rooms/{id}/create', 'RoomController@create');
    Route::get('/dashboard/hotel/rooms/{id}/removeImage', 'RoomController@removeImage');
    Route::post('/dashboard/hotel/{id}/rooms/store', 'RoomController@store');
    Route::get('/dashboard/hotel/switch/room/{id}', 'RoomController@switch');
    Route::put('/dashboard/room/{id}/update', 'RoomController@update');
    Route::get('/dashboard/hotel/room/{id}', 'RoomController@delete');

    Route::get('/dashboard/hotel/{id}/partners', 'PartnerController@index');
    Route::get('/dashboard/hotel/partners/edit/{id}', 'PartnerController@edit');
    Route::get('/dashboard/hotel/partners/{id}/create', 'PartnerController@create');
    Route::get('/dashboard/hotel/partners/{id}/removeImage', 'PartnerController@removeImage');
    Route::post('/dashboard/hotel/{id}/partners/store', 'PartnerController@store');
    Route::put('/dashboard/partner/{id}/update', 'PartnerController@update');
    Route::delete('/dashboard/hotel/partner/{id}', 'PartnerController@delete');
    Route::get('/dashboard/hotel/partner/switch/{id}', 'PartnerController@switch');

    Route::get('/dashboard/hotel/{id}/price', 'PriceController@index');
    Route::put('/dashboard/price/{id}/update', 'PriceController@update');

    Route::get('/dashboard/hotel/{id}/sale', 'SaleController@index');
    Route::put('/dashboard/sale/{id}/update', 'SaleController@update');

    Route::get('/dashboard/hotel/{id}/booking', 'BookingController@index');
    Route::get('/dashboard/hotel/{id}/booking/add', 'BookingController@add');
    Route::get('/dashboard/hotel/{id}/getByDate', 'BookingController@get');
    Route::get('/dashboard/hotel/booking/edit/{id}', 'BookingController@edit');
    Route::get('/dashboard/hotel/booking/{id}/create', 'BookingController@create');
    Route::get('/dashboard/hotel/booking/{id}/switch', 'BookingController@switch');
    Route::post('/dashboard/hotel/{id}/booking/store', 'BookingController@store');
    Route::put('/dashboard/booking/{id}/update', 'BookingController@update');
    Route::get('/dashboard/rooms/booking/{id}', 'BookingController@delete');

    Route::get('/dashboard/product/{id}/params', 'ParamController@index');
    Route::get('/dashboard/product/params/edit/{id}', 'ParamController@edit');
    Route::get('/dashboard/product/params/{id}/create', 'ParamController@create');
    Route::post('/dashboard/product/{id}/params/store', 'ParamController@store');
    Route::put('/dashboard/params/{id}/update', 'ParamController@update');
    Route::delete('/dashboard/product/params/{id}', 'ParamController@delete');

    Route::get('/dashboard/params/{id}/size', 'SizeController@index');
    Route::get('/dashboard/params/size/edit/{id}', 'SizeController@edit');
    Route::get('/dashboard/params/size/{id}/create', 'SizeController@create');
    Route::post('/dashboard/params/{id}/size/store', 'SizeController@store');
    Route::put('/dashboard/size/{id}/update', 'SizeController@update');
    Route::delete('/dashboard/params/size/{id}', 'SizeController@delete');

    Route::put('/dashboard/product/update/{id}', 'ProductController@update');
    Route::delete('/dashboard/product/delete/{id}', 'ProductController@delete');
});
Auth::routes();
