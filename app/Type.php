<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $guarded = [];

    public function hotel()
    {
        return $this->hasMany('App\Hotel');
    }
    public function room_type()
    {
        return $this->hasMany('App\RoomType');
    }
}
