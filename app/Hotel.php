<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    /**
    * The model's guarder.
     * @var array $guarded
     */
    protected $guarded = [];

    protected $appends = ['image'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function review()
    {
        return $this->hasMany('App\Review');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rooms()
    {
        return $this->hasMany('App\Room');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type(){
        return $this->belongsTo('App\Type');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function partners(){
        return $this->belongsTo('App\Partner');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }
    public function getBookingInformation()
    {
        $user = \Auth::user();
        $booking = $this->bookings()->where('user_id',$user->id);
        $adult_count = $booking->pluck('adult')->toArray();
        $room_count = $booking->pluck('room_count')->toArray();
        $price = $booking->pluck('price')->toArray();
        $adult_count = array_sum($adult_count);
        $room_count = array_sum($room_count);
        $price = array_sum($price);
        $price = number_format((float)$price, 2, '.', '');
        return ['adults'=>$adult_count,'rooms'=>$room_count,'price'=>$price];
    }
    public function getImageAttribute()
    {
        $directory = "/uploads/hotel/".$this->id;
        $images = \File::glob($directory . "/*.jpg");
        if(count($images) > 0)
        {
            return $images;
        }
        return [asset('images/hotel-1.png')];
    }
}
