<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded = [];

    public function hotels(){
        return $this->hasMany('App\Hotel');
    }
    public function country(){
        return $this->belongsTo('App\Country');
    }

}
