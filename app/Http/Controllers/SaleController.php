<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\Room;
use Illuminate\Http\Request;
class SaleController extends Controller
{
    public function index($id)
    {
        $data = Hotel::find($id);
        return view('backend.sale.index',compact('data','id'));
    }
    public function update(Request $request, $id)
    {
        $hotel = Hotel::find($id);
        $hotel->update([
            'sale'=> $request->sale
        ]);
        return redirect()->action('SaleController@index',$id)->with('success','Изменения успешно внесены');
    }
}
