<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoomServiceInner;
use App\RoomService;

class RoomServiceInnerController extends Controller
{
    public function index($id)
    {
        $data = RoomServiceInner::where('room_service_id',$id)->get();
        $info = RoomService::find($id);
        return view('backend.room-service-inner.index',compact('data','info'));
    }
    public function create($id)
    {
        return view('backend.room-service-inner.create', compact('id'));
    }
    public function edit($id)
    {
        $data = RoomServiceInner::findOrFail($id);
        return view('backend.room-service-inner.edit',compact('data','id'));
    }
    public function show($id)
    {
        $data = RoomServiceInner::findOrFail($id);
        return view('backend.room-service-inner.show',compact('data'));
    }
    public function store(Request $request,$id)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_en' => 'required',
        ]);
        RoomServiceInner::create([
            'name_ru'=>$request->name_ru,
            'name_en'=>$request->name_en,
            'room_service_id'=> $id,
        ]);
    return redirect()->action('RoomServiceInnerController@index',$id)->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        RoomServiceInner::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('RoomServiceInnerController@index',$id)->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = RoomServiceInner::findOrFail($id);
        $hotel->delete();
        return redirect()->action('RoomServiceInnerController@index',$id)->with('success','Успешно удален');
    }
}
