<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\RoomType;

class RoomTypeController extends Controller
{
    public function index($id)
    {
        $data = RoomType::where('type_id',$id)->get();
        $info = Type::find($id);
        return view('backend.room-type.index',compact('data','info','id'));
    }
    public function create($id)
    {
        return view('backend.room-type.create', compact('id'));
    }
    public function edit($id)
    {
        $data = RoomType::findOrFail($id);
        return view('backend.room-type.edit',compact('data','id'));
    }
    public function show($id)
    {
        $data = RoomType::findOrFail($id);
        return view('backend.room-type.show',compact('data'));
    }
    public function store(Request $request,$id)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_en' => 'required',
        ]);
        RoomType::create([
            'name_ru'=>$request->name_ru,
            'name_en'=>$request->name_en,
            'type_id'=> $id,
        ]);
    return redirect()->action('RoomTypeController@index',$id)->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        RoomType::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('RoomTypeController@index',$id)->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = RoomType::findOrFail($id);
        $hotel->delete();
        return redirect()->action('RoomTypeController@index',$id)->with('success','Успешно удален');
    }
}
