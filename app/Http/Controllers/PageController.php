<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Room;
use App\RoomService;
use App\Service;
use App\Type;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Hotel;
use App\User;
use Auth;
class PageController extends Controller
{
    public function index()
    {
        $hotels = Hotel::all();
        return view('frontend.index',compact('hotels'));
    }
    public function show($id)
    {
        $hotel = Hotel::with('rooms.room_type','rooms.room_name')->findOrFail($id);
        return view('frontend.show',compact('hotel'));
    }
   public function search(Request $request)
    {
        $keyword = $request['keyword'];
        $information = $request['information'];
        $information = explode(',',$information);
        $hotels = Hotel::where('name','like','%'.$keyword.'%')->with('city.country','rooms')->get();
        $type = Type::all();
        $services = Service::with('inners')->get();
        $room_services = RoomService::with('inners')->get();
        $dates = explode('—',$request['date']);
        $from = Carbon::parse($dates[0]);
        $to = Carbon::parse($dates[1]);
        /// filter available rooms
        $hotel_ids = $hotels->pluck('id');
        $from_bookings = Booking::whereIn('hotel_id',$hotel_ids)->whereBetween('from',[$from,$to])->get();
        $to_bookings = Booking::whereIn('hotel_id',$hotel_ids)->whereBetween('to',[$from,$to])->get();
        $booking = $from_bookings->merge($to_bookings);
        $rooms = Room::whereIn('hotel_id',$hotel_ids)->with('room_type','room_name');
        if($request['room_filter']){
            $rooms = $rooms->where('services','like', '%"1"%');
        }
        $rooms = $rooms->get();
        $available_rooms = $rooms->map(function($room) use($booking) {
            $getBookingCount = $booking->where('room_id',$room->id)->count();
            if((($room->room_count * 1) - $getBookingCount) > 0){
                $room->room_count = ($room->room_count * 1) - $getBookingCount;
                return $room;
            }
        });
        $hotel_ids = array_unique($available_rooms->pluck('hotel_id')->toArray());
        $hotels = $hotels->whereIn('id',$hotel_ids);

        return view('frontend.search',compact('hotels','type','services','room_services','keyword','from','to','information'));
    }
    public function getByKeyWord(Request $request){
        $keyword = $request['keyword'];
        $hotels = Hotel::where('name','like','%'.$keyword.'%')->with('city.country')->get();
        return response()->json($hotels);
    }
    public function reservation()
    {
        $user = \Auth::user();
        $rooms = request('rooms');
        $dates = explode(',',request('period'));
        $from = Carbon::parse($dates[0]);
        $to = Carbon::parse($dates[1]);
        $nights = $from->diffInDays($to);
        $parsed_rooms = [];
        $booking_rooms = [];
        $filtered_rooms = [];
        $price = 0;
        foreach($rooms as $key => $room){
            $room = explode(',',$room);
            $getRoom = Room::where('id',$room[1])->with('room_type','room_name')->firstOrFail();
            $hotel = Hotel::findOrFail($room[0]);
            $from_bookings = Booking::where('hotel_id',$hotel->id)->whereBetween('from',[$from,$to])->get();
            $to_bookings = Booking::where('hotel_id',$hotel->id)->whereBetween('to',[$from,$to])->get();
            $booking = $from_bookings->merge($to_bookings);
            $getBookingCount = $booking->where('room_id',$getRoom->id)->count();
            if((($getRoom->room_count * 1) - $getBookingCount) <= 0){
                return redirect()->action('PageController@show',$hotel->id)->with('error','This room'.$getRoom->room_type->name_en.' '.$getRoom->room_name->name_en.'not available');
            }
            array_push($filtered_rooms, $getRoom);
            for($i = 0; $i < $room[2];$i++){
                $changeRoom = $room;
                $changeRoom[2] = 1;
                $changeRoom[4] = $i;
                array_push($booking_rooms, $getRoom);
                array_push($parsed_rooms, $changeRoom);
            }
        }
        foreach($parsed_rooms as $key => $parse){
            $room_count = $parse[2] * 1;
            $person_count = $parse[3] * 1;
            $current_price = (json_decode($booking_rooms[$key]->price,true)[$person_count - 1] * 1) * $room_count;
            $price += $current_price;
            $parsed_rooms[$key][4] = $current_price * $nights; // [hotelId , roomId, roomCount, personCount, price]
        }
        $hotel = $booking_rooms[0]->hotel;
        $booking_rooms = $filtered_rooms;
        $parsed_rooms = json_encode($parsed_rooms);
        $booking_rooms = json_encode($booking_rooms);
        $price = $price * $nights;
        $tution = $price / 100 * 2.5;
        return view('frontend.reservation',compact('user','hotel','from','to','booking_rooms','parsed_rooms','price','tution','nights'));
    }
    public function book(Request $request)
    {
        if(!Auth::check()){
            $request['password'] = bcrypt('secret');
            $this->validate($request, [
                'email' => 'required|unique:users,email',
                'name' => 'required',
                'phone' => 'required|unique:users',
                'country' => 'required',
            ]);
            $user = User::create(request()->only('name','email','phone','password'));
            auth()->login($user);
        }
        $period = explode(',',$request->period); // [ from , to ]
        $from = Carbon::parse($period[0]);
        $to = Carbon::parse($period[1]);
        $nights = $from->diffInDays($to);
        $user = Auth::user();
        $booking_rooms = $request->room_info; /// should be added checking status
        foreach ($booking_rooms as $key => $room){
            $exploded = explode(',',$room); // [hotelId,roomId, roomCount, personCount, price]
            $room = Room::findOrFail($exploded[1]);
            $hotel = Hotel::findOrFail($exploded[0]);
            $from_bookings = Booking::where('hotel_id',$hotel->id)->whereBetween('from',[$from,$to])->get();
            $to_bookings = Booking::where('hotel_id',$hotel->id)->whereBetween('to',[$from,$to])->get();
            $booking = $from_bookings->merge($to_bookings);
            $getBookingCount = $booking->where('room_id',$room->id)->count();
            if((($room->room_count * 1) - $getBookingCount) <= 0){
                return redirect()->action('PageController@show',$hotel->id)->with('error','This room'.$room->room_type->name_en.' '.$room->room_name->name_en.'not available');
            }
            Booking::create([
                'from'=> $from,
                'to'=> $to,
                "user_id" => $user->id,
                "room_id" => $room->id,
                "hotel_id" => $room->hotel_id,
                "numeration" => 0,
                "name" => $request->guest_name[$key],
                "adult" => 1,
                "room_count" => $exploded[2],
                "child" => 0,
                "price" => (json_decode($room->price,true)[((1 * 1) - 1)] * $exploded[2] *  $nights),
                "phone" => $user->phone,
                "email" => $user->email
            ]);
        }
        return redirect()->action('PageController@confirmation', $room->hotel->id);
    }
    public function confirmation($hotelId)
    {
        $booking = Booking::where('hotel_id',$hotelId)->get();
        return view('frontend.confirmation',compact('booking'));
    }
    public function bookings()
    {
        if(!Auth::check()){
            return redirect()->action('PageController@index');
        }
        $user = Auth::user();
        $bookings = $user->bookings->pluck('hotel_id')->toArray();
        $hotel_ids = array_unique($bookings);
        $hotels = Hotel::whereIn('id',$hotel_ids)->get();
        if(count($hotels) == 0){
            return view('frontend.empty');
        }
        return view('frontend.bookings', compact('hotels'));
    }
    public function details($id)
    {
        if(!Auth::check()){
            return redirect()->action('PageController@index');
        }
        $user = Auth::user();
        $bookings = $user->bookings()->where('hotel_id',$id)->get();
        $hotel = Hotel::findOrFail($id);
        return view('frontend.details',compact('bookings','hotel'));
    }
    public function change($id)
    {
        if(!Auth::check()){
            return redirect()->action('PageController@index');
        }
        $user = Auth::user();
        $booking = Booking::where([['id',$id],['user_id',$user->id]])->firstOrFail();
        $from = Carbon::parse($booking->from);
        $to = Carbon::parse($booking->to);
        $nights = $from->diffInDays($to);
        return view('frontend.change', compact('booking','nights'));
    }
    public function cancel($id)
    {
       if(!Auth::check()){
            return redirect()->action('PageController@index');
        }
        $user = Auth::user();
        $bookings = Booking::where([['id',$id],['user_id',$user->id]])->get();
        return view('frontend.cancel',compact('bookings'));
    }
    public function cancelHotel($id)
    {
       if(!Auth::check()){
            return redirect()->action('PageController@index');
        }
        $user = Auth::user();
        $bookings = Booking::where([['hotel_id',$id],['user_id',$user->id]])->get();
        return view('frontend.cancel',compact('bookings'));
    }
    public function newDates($id)
    {
        $booking = Booking::findOrFail($id);
        $from = Carbon::parse($booking->from);
        $to = Carbon::parse($booking->to);
        $old_nights = $from->diffInDays($to);
        $from = Carbon::parse(request('from'));
        $to = Carbon::parse(request('to'));
        $nights = $from->diffInDays($to);
        $price = number_format((float)$booking->price / $old_nights * $nights, 2, '.', '');
        return view('frontend.new-dates',compact('booking','from','to','nights','price'));
    }
    public function changeBook(Request $request, $id){
        if(!Auth::check()){
            return redirect()->action('PageController@index');
        }
        $user = Auth::user();
        $dates = explode(',',request('dates'));
        $booking = Booking::where([['id',$id],['user_id',$user->id]])->firstOrFail();
        $hotel = $booking->hotel;
        $from = Carbon::parse($booking->from);
        $to = Carbon::parse($booking->to);
        $old_nights = $from->diffInDays($to);
        $from = Carbon::parse($dates[0]);
        $to = Carbon::parse($dates[1]);
        $nights = $from->diffInDays($to);
        $price = (float)$booking->price / $old_nights * $nights;
        $booking->update([
            'from'=> $from,
            'to'=> $to,
            'price'=> $price
        ]);
        return redirect()->action('PageController@details',$hotel->id)->with('success','Changes successfully made');
    }
    public function setCancel()
    {
        if(!Auth::check()){
            return redirect()->action('PageController@index');
        }
        $user = Auth::user();
        $booking_ids = \request('booking_id');
        Booking::withoutTrashed()->where('user_id', $user->id)
          ->where(function ($query) use ($booking_ids) {
              $query->whereIn('id',$booking_ids);
          })->delete();
        return redirect()->action('PageController@bookings')->with('success','Selected bookings successfully deleted');
    }
    public function settings()
    {
        return view('frontend.settings');
    }
    public function empty()
    {
        return view('frontend.empty');
    }
}
