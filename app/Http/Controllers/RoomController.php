<?php

namespace App\Http\Controllers;

use App\RoomType;
use Illuminate\Http\Request;
use App\Hotel;
use App\Room;
use App\Type;
use App\RoomName;
use App\Bed;
use App\RoomService;
use Auth;

use Intervention\Image\ImageManagerStatic as Image;

class RoomController extends Controller
{
    public function index($id)
    {
        $data = Room::where('hotel_id',$id)->get();
        return view('backend.room.index',compact('data','id'));
    }
    public function create($id)
    {
        $hotel = Hotel::findOrFail($id);
        $room_type = Type::where('id',$hotel->type_id)->first()->room_type()->get();
        $room_type_ids = [];
        foreach ($room_type as $type){
            array_push($room_type_ids,$type->id);
        }
        $name = RoomName::whereIn('type_id',$room_type_ids)->get();
        $bed = Bed::all();
        $service = RoomService::all();
        return view('backend.room.create',compact('id','name','room_type','bed','service'));
    }
    public function edit($id)
    {
        $data = Room::findOrFail($id);
        $service = RoomService::all();
        $bed = Bed::all();
        $directory = "uploads/room/".$id;
        $images = \File::glob($directory . "/*.jpg");
        $id = $data->hotel->id;
        return view('backend.room.edit',compact('data','service','bed','id','images'));
    }
    public function show($id)
    {
        $data = Room::findOrFail($id);
        return view('backend.room.show',compact('data'));
    }
    public function store(Request $request, $id)
    {
        request()->validate([
            "room_type_id" => 'required',
            "room_count" => 'required',
            "capacity" => 'required',
            "price" => 'required',
            "numeration" => 'required',
            "bed" => 'required',
            "bed_count" => 'required',
        ]);
        $capacity = $request->capacity;
        $price = array_slice($request->price,0,$capacity);
        $bed_count = $request->bed_count;
        $bed = array_slice($request->bed,0 ,$bed_count);
        $room_count = $request->room_count;
        $numeration = array_slice($request->numeration,0,$room_count);
        $services = $request->service;
        $selected = [];
        if($services){
            foreach ($services as $service){
                $decoded = json_decode($service);
                if($decoded[0] != 0){
                    $selected[ucfirst($decoded[1])] = $decoded[0];
                }
            }
        }
        $checker = Room::where(["room_type_id" => $request->room_type_id,"room_name_id" => $request->room_name_id,'hotel_id'=> $id])->first();
        if($checker){
            return redirect()->back()->with('success','Такой тип номера уже сушествует');
        }
        $room = Room::create([
            "room_type_id" => $request->room_type_id,
            "room_name_id" => $request->room_name_id,
            "room_count" => $request->room_count,
            "capacity" => $capacity,
            "price" => json_encode($price,true),
            "numeration" => json_encode($numeration,true),
            "numeration_enabled" => $request->numeration_enabled,
            "bed" => json_encode($bed,true),
            "bed_count" => json_encode($bed_count,true),
            "services" => json_encode($selected,true),
            'hotel_id'=> $id,
        ]);
        $this->createFolder('room',$room->id);
        $images = $request->file('image');
        $this->storeImages($images,$room->id);
        if($request->confirm == 'more'){
            return redirect()->back()->with('success','Успешно добавлено');
        }
    return redirect()->action('RoomController@index',$id)->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        $services = $request->service;
        $selected = [];
        $room = Room::find($id);
        $capacity = $room->capacity;
        $price = array_slice($request->price,0,$capacity);
        $numeration = $request->numeration;
        if($services){
            foreach ($services as $service){
                $decoded = json_decode($service);
                if($decoded[0] != 0){
                    $selected[ucfirst($decoded[1])] = $decoded[0];
                }
            }
        }
        $services = $request->service;
        $selected = [];
        if($services){
            foreach ($services as $service){
                $decoded = json_decode($service);
                if($decoded[0] != 0){
                    $selected[ucfirst($decoded[1])] = $decoded[0];
                }
            }
        }
        $bed_count = $request->bed_count;
        $bed = array_slice($request->bed,0 ,$bed_count);
        $room->update([
            "price" => json_encode($price,true),
            "numeration" => $numeration ? json_encode($numeration,true) : $room->numeration,
            "bed" => json_encode($bed,true),
            "bed_count" => json_encode($bed_count,true),
            "services" => json_encode($selected,true),
        ]);
        $this->createFolder('room',$id);
        $images = $request->file('image');
        if($images){
            $this->storeImages($images,$id);
        }
        return redirect()->action('RoomController@index',$room->hotel->id)->with('success','Изменения успешно внесены');
    }
    public function switch(Request $request, $id)
    {
        $room = Room::findOrFail($id);
        $room->update([
            'status'=> !$room->status
        ]);
        return redirect()->action('RoomController@index',$room->hotel->id)->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $room = Room::findOrFail($id);
        $hotel_id = $room->hotel->id;
        $room->booking()->delete();
        $room->delete();
        return redirect()->action('RoomController@index',$hotel_id)->with('success','Успешно удален');
    }
    public function createFolder($directory,$roomId)
    {
        $path = public_path('uploads/'.$directory.'/'.$roomId);
        $directory_path = public_path('uploads/'.$directory);
        if(!\File::isDirectory($directory_path)){
            \File::makeDirectory($directory_path,0777,true,true);
        }
        if(!\File::isDirectory($path)){
            \File::makeDirectory($path,0777,true,true);
        }
        return 1;
    }
    public function storeImages($images,$roomId)
    {
        foreach($images as $key => $image){
            $filename = $key+strtotime("now") .'.jpg';
            $path = public_path('uploads/room/'.$roomId.'/'. $filename);
            Image::make($image->getRealPath())->encode('jpg', 90)
            ->fit(945, 630)
            ->save($path);
        }
        return 1;
    }
    public function removeImage($id)
    {
        $file_name = request('file_name');
        $pathToDestroy = public_path('uploads/room/'.$id.'/'.$file_name);
        \File::delete($pathToDestroy);
        return response('okay',200);
    }
}
