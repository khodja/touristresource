<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Hotel;
use App\City;
use App\User;
use App\Type;
use App\Service;
use Intervention\Image\ImageManagerStatic as Image;

class HotelController extends Controller
{
    public function create()
    {
        $data = City::all();
        $user = User::all();
        $country = Country::all();
        $type = Type::all();
        $service = Service::all();
        $begin = new \DateTime("06:00");
        $end   = new \DateTime("24:00");
        $interval = \DateInterval::createFromDateString('30 min');
        $times    = new \DatePeriod($begin, $interval, $end);
        return view('backend.hotel.create', compact('data','user','country','type','service','times'));
    }
    public function edit($id)
    {
        $current_hotel = Hotel::findOrFail($id);
        $city = City::all();
        $directory = "uploads/hotel/".$id;
        $images = \File::glob($directory . "/*.jpg");
        $country = Country::all();
        $user = User::all();
        $type = Type::all();
        $service = Service::all();
        $begin = new \DateTime("06:00");
        $end   = new \DateTime("24:00");
        $interval = \DateInterval::createFromDateString('30 min');
        $times    = new \DatePeriod($begin, $interval, $end);
        return view('backend.hotel.edit',compact('current_hotel','id','city','country','user','service','type','times','images'));
    }
    public function show($id)
    {
        $data = Hotel::findOrFail($id);
        return view('backend.hotel.show',compact('data'));
    }
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'city_id'=>'required',
            'type_id'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'income'=> 'required',
            'outcome'=> 'required',
            'animals'=> 'required'
        ]);
        $services = $request->service;
        $selected = [];
        if($services){
            foreach ($services as $service){
                $decoded = json_decode($service);
                if($decoded[0] != 0){
                    $selected[ucfirst($decoded[1])] = $decoded[0];
                }
            }
        }
        $hotel = Hotel::create([
            'name'=>$request->name,
            'city_id'=>$request->city_id,
            'user_id'=>$request->user_id,
            'type_id'=>$request->type_id,
            'address'=>$request->address,
            'phone'=>$request->phone,
            'income'=> $request->income,
            'income_to'=> $request->income_to,
            'outcome'=> $request->outcome,
            'outcome_to'=> $request->outcome_to,
            'confirmation'=> $request->confirmation ? $request->confirmation : 0,
            'credit'=> $request->credit ? $request->credit : 0,
            'cards'=> $request->cards ? json_encode($request->cards) : json_encode([]),
            'animals'=> $request->animals,
            'services'=> json_encode($selected),
            'status'=> 0,
            'lat'=>$request->lat,
            'long'=>$request->long,
        ]);
        $this->createFolder('hotel',$hotel->id);
        $images = $request->file('image');

        $this->storeImages($images,$hotel->id);
        return redirect()->action('RoomController@create',$hotel->id)->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        $services = $request->service;
        $selected = [];
        if($services){
            foreach ($services as $service){
                $decoded = json_decode($service);
                if($decoded[0] != 0){
                    $selected[ucfirst($decoded[1])] = $decoded[0];
                }
            }
        }
        Hotel::findOrFail($id)->update([
            'name'=>$request->name,
            'city_id'=>$request->city_id,
            'type_id'=>$request->type_id,
            'address'=>$request->address,
            'phone'=>$request->phone,
            'rating'=>$request->rating,
            'income'=> $request->income,
            'outcome'=> $request->outcome,
            'confirmation'=> $request->confirmation ? $request->confirmation : 0,
            'credit'=> $request->credit ? $request->credit : 0,
            'cards'=> json_encode($request->cards),
            'animals'=> $request->animals,
            'services'=> json_encode($selected),
            'status'=> 0,
            'lat'=>$request->lat,
            'long'=>$request->long,
        ]);
        $this->createFolder('hotel',$id);
        $images = $request->file('image');
        if($images){
            $this->storeImages($images,$id);
        }
        return redirect()->action('HomeController@index')->with('success','Изменения успешно внесены');
    }
    public function switch(Request $request, $id)
    {
        $hotel = Hotel::findOrFail($id);
        $hotel->update([
            'status'=> !$hotel->status
        ]);
        return redirect()->action('HomeController@index')->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = Hotel::findOrFail($id);
        $hotel->delete();
        return redirect()->action('HomeController@index')->with('success','Успешно удален');
    }
    public function createFolder($directory,$hotelId)
    {
        $path = public_path('uploads/'.$directory.'/'.$hotelId);
        $directory_path = public_path('uploads/'.$directory);
        if(!\File::isDirectory($directory_path)){
            \File::makeDirectory($directory_path,0777,true,true);
        }
        if(!\File::isDirectory($path)){
            \File::makeDirectory($path,0777,true,true);
        }
        return 1;
    }
    public function storeImages($images,$hotelId)
    {
        foreach($images as $key => $image){
            $filename = $key+strtotime("now") .'.jpg';
            $path = public_path('uploads/hotel/'.$hotelId.'/'. $filename);
            Image::make($image->getRealPath())->encode('jpg', 90)
            ->fit(945, 630)
            ->save($path);
        }
        return 1;
    }
    public function removeImage($id)
    {
        $file_name = request('file_name');
        $pathToDestroy = public_path('uploads/hotel/'.$id.'/'.$file_name);
        \File::delete($pathToDestroy);
        return response('okay',200);
    }
}
