<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\City;

class CityController extends Controller
{
    public function index($id)
    {
        $data = City::where('country_id',$id)->get();
        return view('backend.city.index',compact('data','id'));
    }
    public function create($id)
    {
        return view('backend.city.create',compact('id'));
    }
    public function edit($id)
    {
        $data = City::find($id);
        return view('backend.city.edit',compact('data','id'));
    }
    public function store(Request $request, $id)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_en' => 'required',
        ]);
        City::create([
            'name_ru' => $request->name_ru,
            'name_en' => $request->name_en,
            'country_id' => $id,
        ]);
        return redirect()->action('CityController@index')->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_en' => 'required',
        ]);
        City::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('CityController@index')->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = City::findOrFail($id);
        $hotel->delete();
        return redirect()->action('CityController@index')->with('success','Успешно удален');
    }
}
