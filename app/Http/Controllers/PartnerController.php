<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
use App\Partner;

class PartnerController extends Controller
{
    public function index($id)
    {
        $data = Partner::where('hotel_id',$id)->get();
        return view('backend.partner.index',compact('data','id'));
    }
    public function create($id)
    {
        $hotel = Hotel::findOrFail($id);
        return view('backend.partner.create',compact('id','hotel'));
    }
    public function edit($id)
    {
        $data = Partner::findOrFail($id);
        $id = $data->hotel_id;
        return view('backend.partner.edit',compact('data','id'));
    }
    public function show($id)
    {
        $data = Partner::findOrFail($id);
        return view('backend.partner.show',compact('data'));
    }
    public function store(Request $request, $id)
    {
        request()->validate([
            "name" => 'required',
            "phone" => 'required',
            "email" => 'required',
        ]);
        $create_data = $request->all();
        $create_data['hotel_id'] = $id;
        $create_data['enabled'] = true;
        Partner::create($create_data);

    return redirect()->action('PartnerController@index',$id)->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        Partner::findOrFail($id)->update($request->all());
        return redirect()->action('PartnerController@index',$id)->with('success','Изменения успешно внесены');
    }
    public function switch(Request $request, $id)
    {
        $partner = Partner::findOrFail($id);
        $partner->update([
            'enabled'=> !$partner->enabled
        ]);
        return redirect()->action('PartnerController@index',$id)->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $partner = Partner::findOrFail($id);
        $hotel_id = $partner->hotel_id;
        $partner->delete();
        return redirect()->action('PartnerController@index',$hotel_id)->with('success','Успешно удален');
    }
    public function createFolder($directory,$roomId)
    {
        $path = public_path('uploads/'.$directory.'/'.$roomId);
        $directory_path = public_path('uploads/'.$directory);
        if(!\File::isDirectory($directory_path)){
            \File::makeDirectory($directory_path,666,true);
        }
        if(!\File::isDirectory($path)){
            \File::makeDirectory($path,666,true);
        }
        return 1;
    }
    public function storeImages($images,$roomId)
    {
        foreach($images as $key => $image){
            $filename = $key+strtotime("now") .'.jpg';
            $path = public_path('uploads/room/'.$roomId.'/'. $filename);
            Image::make($image->getRealPath())->encode('jpg', 90)
            ->fit(945, 630)
            ->save($path);
        }
        return 1;
    }
    public function removeImage($id)
    {
        $file_name = request('file_name');
        $pathToDestroy = public_path('uploads/room/'.$id.'/'.$file_name);
        \File::delete($pathToDestroy);
        return response('okay',200);
    }
}
