<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Hotel;
use Auth;
use App\Http\View\Composers\HotelComposer;
class ComposerServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('*', HotelComposer::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
