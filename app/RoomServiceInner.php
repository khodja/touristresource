<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomServiceInner extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo('App\RoomService');
    }
}
