const mix = require('laravel-mix');
// const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/styles.scss', 'public/css');
// mix.browserSync({
//     proxy: 'localhost:8000',
//            files: [
//                'app/**/*',
//                'public/**/*',
//                'resources/views/**/*',
//                'routes/**/*'
//            ]
// });
